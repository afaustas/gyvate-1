/* ===================================================================================
    GYVATE v1.0 (aprasyma ziureti main.c faile arba dokumentacijoje)
   ===================================================================================
    FAILAS: SCORE_CTRL.H
    PASKIRTIS: Tasku control'as. Jo sukurimas, ir funkcijos.
   -----------------------------------------------------------------------------------
    Kompiliatorius: gcc (TDM-2 mingw32) 4.4.1
    Faustas - Azuolas BAGDONAS, 2011-12
   ===================================================================================  */

#ifndef __SCORE_CTL_H
#define __SCORE_CTL_H

#include <windows.h>
#include <wingdi.h>
#include "boolean.h"
#include "resource.h"
#include "gamealg.h"

#define SCORE_CTRL_CLASS_NAME   "ScoreCtrl"

/*#define IDB_SOUND 0 - liko nenaudojami, nes valdoma klaviatura.
#define IDB_MENU  1 */
#define IDC_SCORE 2

char g_scoreword[16]; /* Sia saugomas zodis "TASKAI" */

BOOL InitScoreCtrl(void);
LRESULT CALLBACK ScoreCtrlProc (HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
void PaintOnScore(unsigned int score, unsigned int bonust, HWND hScoreWnd, HDC hDCScore);
void RefreshWindow(HWND hWnd);

#endif
