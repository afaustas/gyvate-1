/* ===================================================================================
    GYVATE v1.0 (aprasyma ziureti main.c faile arba dokumentacijoje)
   ===================================================================================
    FAILAS: GAMETHREAD.H
    PASKIRTIS: Zaidimo threadas, jo funkcijos. Pauzes funkcija. Zaidimo grafines situa-
    cijos piesimo funkcijos, kurias naudoja thread'as, kad butu galima atvaizduoti zai-
    dima.
   -----------------------------------------------------------------------------------
    Kompiliatorius: gcc (TDM-2 mingw32) 4.4.1
    Faustas - Azuolas BAGDONAS, 2011-12
   ===================================================================================  */
#ifndef __THREAD_H
    #define __THREAD_H
    #include <windows.h>
    #include <windowsx.h>
    #include <mmsystem.h>
    #include "gamealg.h"
    #include "score_ctrl.h"
    #include "settings.h"
    #include "records.h"

    int g_turn; /* Posukis. Galimos reiksmes: LEFT, RIGHT, UP, DOWN, NONE */
    BOOL g_started_game; /* Jei zaidimas prasidejes igyja reiksme TRUE */
    BOOL g_paused_game; /* Jei pauze nuspausta */
    extern HWND g_hMainWnd; /* Handleris i pagrindini zaidimo langa */
    extern char g_lvlpath[MAX_PATH]; /* Kelias iki pasirinkto lygio failo */

    HANDLE CreateGameThread(HDC hDC);
    DWORD WINAPI GameThreadProc(HDC hDC);
    /* BOOL IsTurned(void); - nebepamenu kam. Nenaudojama. */
    void RepaintAll(HDC hSheadDC,  HDC hBodyDC, HDC hEndDC, HDC hTurnDC, HDC hFoodDC, HDC hWallDC, HDC hBgDC, HDC hWndDC);
    void PauseGame(HANDLE hThread, HWND hWnd, HDC hDC);
    BOOL RotateDC(HDC hDC, int Width, int Height, int angle);
    void PutSnakePart(HDC hdcMem, int x, int y, HDC hdcPart);

#endif
