/* ===================================================================================
    GYVATE v1.0 (aprasyma ziureti main.c faile arba dokumentacijoje)
   ===================================================================================
    FAILAS: RECORDS.H
    PASKIRTIS: Kas susije su rekordais, ju gavimu, saugojimu, rodymu.
   -----------------------------------------------------------------------------------
    Kompiliatorius: gcc (TDM-2 mingw32) 4.4.1
    Faustas - Azuolas BAGDONAS, 2011-12
   ===================================================================================  */
#ifndef __RECORDS_H
#define __RECORDS_H
#include "boolean.h"
#include "resource.h"
#include "settings.h"
#include <windows.h>
#include <stdio.h>

typedef struct _record{char name[50]; unsigned short int record;} RECORD;
//RECORD records[6]; /* Pirmi trys  - klasikiniai, o kiti - arkade. Nerealizuota sioje ver. */
RECORD record;
extern char g_lvlpath[MAX_PATH];
extern unsigned int score;
//extern SETTINGS sets;

BOOL CALLBACK RecordDlgProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
RECORD GetRecord(char *lev_path);
BOOL SetRecord(RECORD *record, char *lev_path);
void DisplayRecord(char *lev_path);

#endif
