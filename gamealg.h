/* ===================================================================================
    GYVATE v1.0 (aprasyma ziureti main.c faile arba dokumentacijoje)
   ===================================================================================
    FAILAS: GAMEALG.H
    PASKIRTIS: Cia saugomas zaidimo algoritmas. Su WinAPI nesusijes, todel laisvai gali
    buti keliamas ant kitos platformos (portable).
   -----------------------------------------------------------------------------------
    Kompiliatorius: gcc (TDM-2 mingw32) 4.4.1
    Faustas - Azuolas BAGDONAS, 2011-12
   ===================================================================================  */
#ifndef __GAMEALG_H
 #define __GAMEALG_H

 #define FNAME   "\\gyvate.sav"  /* Save failo vardas */
 #define SYSFOLD "appdata" /* Sisteminis kintamasis, nurodantis sistemine direktorija, kur bus saugojamas save zaidimo failas */

 #include "boolean.h"
 #include "stdio.h"
 #include "memory.h"

/* Koordinaciu struktura */
typedef struct _coord{int x, y;} COORDSN;
/* Gyvates struktura */
typedef struct _snake{
					  COORDSN data[64*48]; /* Daliu koordinates */
					  unsigned int len; /* Ilgis */
					 } SNAKE;

enum turns {LEFT, RIGHT, UP, DOWN, NONE}; /* Galimi pasukimai */
SNAKE snk; /* Gyvates informacija saugoma cia */
char screen[48][64];
unsigned int score, scoreinc; /* score - taskai; scoreinc - per kiek tasku didinti rezultata, kai suvalgytas maistas */

BOOL loadlevel(char *fname);
//void loadsnake(void);
void load_snake(void);
//void upd_snake(void);
BOOL upd_snake(int turn);
void put_food(void);
BOOL save_game(char *levelpath);
BOOL restore_game(char *levelpath);
BOOL saved_game_exist(void);
#endif
