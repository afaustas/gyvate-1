/* ===================================================================================
    GYVATE v1.0 (aprasyma ziureti main.c faile arba dokumentacijoje)
   ===================================================================================
    FAILAS: MAIN.H
    PASKIRTIS: Atsakingas uz UI, pagrindinio lango sukurima, piesima jame, meniu, etc.
   -----------------------------------------------------------------------------------
    Kompiliatorius: gcc (TDM-2 mingw32) 4.4.1
    Faustas - Azuolas BAGDONAS, 2011-12
   ===================================================================================  */

#ifndef __MAIN_H
 #define __MAIN_H

#include <windows.h>
#include <wingdi.h>
#include <stdio.h>
#include <time.h>
#include <commctrl.h>
#include "resource.h"
#include "gamethread.h"
#include "boolean.h"
#include "gamealg.h"
#include "score_ctrl.h"
#include "settings.h"
#include "records.h"

#define TRUE  1
#define FALSE 0

/*  Funkciju deklaracijos  */
LRESULT CALLBACK WndProc (HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK WndStatusProc (HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
void error(int err_res, HWND hWnd, HINSTANCE hInstance);
HWND create_main_window(HINSTANCE hInstance, HINSTANCE hPrevInstance, int nCmdShow, MSG *Mgs, HBRUSH hBrBg, BOOL bFullScreen);
BOOL set_fullscreen_resolution(void);
HWND create_status(HWND hParent);
int MainMenu(HWND hParent);
BOOL CALLBACK MMDlgProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
BOOL CALLBACK AboutDlgProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
void Music(void);

/*  Globalus kintamieji */
char g_szClassName[ ] = "Gyvate_MainWindow"; /* Lango klases vardas */
char g_szStatusClassName[ ] = "Gyvate_StatusWindow"; /* Busenos lango klases vardas */
char g_szGenStr[2048]; /* Bendros paskirties, stringu ikrovimui is resursu */
char g_lvlpath[MAX_PATH]; /* Cia saugomas kelias iki pasirinkto lygio. Reikalingas, kad zinoti, kur rekordus saugoti */
HWND g_hMainWnd;

HWND hStatusWnd; /* Buvo planuota keisti, daryt neglobalini, bet taip ir liko */
#endif
