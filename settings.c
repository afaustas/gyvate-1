/* ===================================================================================
    GYVATE v1.0 (aprasyma ziureti main.c faile arba dokumentacijoje)
   ===================================================================================
    FAILAS: SETTINGS.C
    PASKIRTIS: Nustatymu dialogas ir jo funkcijos. Issaugojimas ir atstatymas nustatymu
    is registro.
   -----------------------------------------------------------------------------------
    Kompiliatorius: gcc (TDM-2 mingw32) 4.4.1
    Faustas - Azuolas BAGDONAS, 2011-12
   ===================================================================================  */

#include "settings.h"

/* Nustatymu dialogo procedura */
BOOL CALLBACK SettingsDlgProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam){
    switch(msg){
        /* Initializacija dialogo control'u. Pagal esamus nustatymus reikia uzpildyti control'us. */
        case WM_INITDIALOG:
        {
            HWND hInitCtrl;

            hInitCtrl = GetDlgItem(hwnd, MAKEINTRESOURCE(IDC_SSPEED));
            SendMessage(hInitCtrl, TBM_SETRANGE, (WPARAM) TRUE, (LPARAM) MAKELONG(1, 100));
            SendMessage(hInitCtrl, TBM_SETPAGESIZE, 0, (LPARAM) 1);
            SendMessage(hInitCtrl, TBM_SETPOS, (WPARAM) TRUE, (LPARAM) sets.speed);

            hInitCtrl = GetDlgItem(hwnd, MAKEINTRESOURCE(IDC_EDTMUSFILENAME));
            SetWindowText(hInitCtrl, (LPARAM)sets.musfile);

            hInitCtrl = GetDlgItem(hwnd, MAKEINTRESOURCE(IDC_SNDON));
            if(sets.sndon) Button_SetCheck(hInitCtrl, BST_CHECKED);

            hInitCtrl = GetDlgItem(hwnd, MAKEINTRESOURCE(IDC_MUSON));
            if(sets.muson) Button_SetCheck(hInitCtrl, BST_CHECKED);

            hInitCtrl = GetDlgItem(hwnd, MAKEINTRESOURCE(IDC_CHKFULLSCR));
            if(sets.fullscreen) Button_SetCheck(hInitCtrl, BST_CHECKED);

            CheckRadioButton(hwnd, MAKEINTRESOURCE(IDC_RADUSESD),MAKEINTRESOURCE(IDC_RADLOADMUSFILE),MAKEINTRESOURCE((sets.custommus)?(IDC_RADLOADMUSFILE):(IDC_RADUSESD)));
            if(!sets.custommus){
                EnableWindow(GetDlgItem(hwnd, MAKEINTRESOURCE(IDC_BMUSFLOAD)), FALSE);
                EnableWindow(GetDlgItem(hwnd, MAKEINTRESOURCE(IDC_EDTMUSFILENAME)), FALSE);
            }
        }
            break;
        case WM_COMMAND:
            switch(LOWORD(wParam))
            {
                case IDC_BMUSFLOAD: /* Nuspaustas "Browse" mygtukas. */
                {
                    char filterbuf[256], dialogcaption[64], szName[MAX_PATH], x, len;
                    OPENFILENAME ofn;

                    LoadString(GetModuleHandle(NULL), IDS_MUSFILTERSTR, filterbuf, 255); /* Filtrai */
                    LoadString(GetModuleHandle(NULL), IDS_LOADMUSCAPT, dialogcaption, 63); /* Dialogo lango caption */

                    /* Siekiant apsisaugoti nuo problemu su resursais, kai yra null terminatoriai ir stringu viduryje */
                    len = strlen(filterbuf);
                    for(x = 0; x < len; x++){
                        if(filterbuf[x] == '_'){
                            filterbuf[x] = '\0';
                        }
                        putchar(filterbuf[x]);
                    }

                    /* Open File standartinio dialogo strukturos initializacija */
                    ZeroMemory(&ofn,sizeof(OPENFILENAME));
                    ofn.lStructSize = sizeof(ofn);
                    ofn.hwndOwner = hwnd;
                    ofn.lpstrFilter = filterbuf;
                    ofn.nFilterIndex=1;
                    ofn.nMaxFile = sizeof(szName);
                    ofn.Flags = OFN_EXPLORER;
                    ofn.lpstrTitle = dialogcaption;
                    ofn.lpstrFile = szName;
                    ofn.lpstrFile[0] = '\0';
                    ofn.Flags = OFN_PATHMUSTEXIST|OFN_FILEMUSTEXIST ;

                    /* Rodomas dialogas ir saugomas rezultatas, jei nepaspausta CANCEL */
                    if (GetOpenFileName((LPOPENFILENAME)&ofn)){
                        HWND hEdtFName;
                        strcpy(sets.musfile, szName);
                        #ifdef DEBUG
                        printf("Pairinktas failas: %s \n", sets.musfile);
                        #endif
                        hEdtFName = GetDlgItem(hwnd, MAKEINTRESOURCE(IDC_EDTMUSFILENAME));
                        SetWindowText(hEdtFName, (LPARAM)sets.musfile);
                    }

                }
                    break;
                case IDOK: /* Nuspaustas mygtukas OK. Cia surasomi nustatymai i atminti (sets struktura) */
                {
                    HWND hCurCtrl = GetDlgItem(hwnd, MAKEINTRESOURCE(IDC_SSPEED));
                    sets.speed = SendMessage(hCurCtrl, TBM_GETPOS, 0, 0);
                    #ifdef DEBUG
                    printf("Speed: %d\n", sets.speed);
                    #endif

                    /* Checkboxai */
                    hCurCtrl = GetDlgItem(hwnd, MAKEINTRESOURCE(IDC_SNDON));
                    sets.sndon = Button_GetCheck(hCurCtrl);

                    hCurCtrl = GetDlgItem(hwnd, MAKEINTRESOURCE(IDC_MUSON));
                    sets.muson = Button_GetCheck(hCurCtrl);

                    hCurCtrl = GetDlgItem(hwnd, MAKEINTRESOURCE(IDC_CHKFULLSCR));
                    sets.fullscreen = Button_GetCheck(hCurCtrl);

                    hCurCtrl = GetDlgItem(hwnd, MAKEINTRESOURCE(IDC_EDTMUSFILENAME));
                    sets.musfile[0] = MAX_PATH - 1;
                    sets.musfile[SendMessage(hCurCtrl, EM_GETLINE, 0, (LPARAM)sets.musfile)] = 0;

                    sets.custommus = (IsDlgButtonChecked(hwnd, MAKEINTRESOURCE(IDC_RADLOADMUSFILE)) == BST_CHECKED);

                    Music(); /* Jei pakeisti muzikos nustatymai, aktyvuoja naujus */

                    DestroyWindow(hwnd);
                }
                    break;
                case IDCANCEL: /* Atsaukta */
                    DestroyWindow(hwnd);
                    break;
                case IDC_RADUSESD:       /* Jei  del muzikos paspaustas kuris nors radio buttonas */
                case IDC_RADLOADMUSFILE:
                    {
                        HWND hButton, hEdit;
                        /* Gaunu Browse ir Edit, i kuri vedamas failo vardas, handlerius */
                        hButton = GetDlgItem(hwnd, MAKEINTRESOURCE(IDC_BMUSFLOAD));
                        hEdit = GetDlgItem(hwnd, MAKEINTRESOURCE(IDC_EDTMUSFILENAME));

                        /* Priklausomai nuo to, kuris radio button'as pazymetas, ijungiu arba isjungiu Browse ir Edit controls */
                        if(IsDlgButtonChecked(hwnd, MAKEINTRESOURCE(IDC_RADLOADMUSFILE)) == BST_CHECKED){
                            EnableWindow(hButton, TRUE);
                            EnableWindow(hEdit, TRUE);
                        }
                        else{
                            EnableWindow(hButton, FALSE);
                            EnableWindow(hEdit, FALSE);
                        }
                    }
            }
            break;
        /* case WM_DESTROY:
            break; */
        default: return FALSE;
    }
    return TRUE;
}

/* ----------------------------------------------------------------------------------------------
    Funkcija  : GetSettings()
    Aprasymas : Ikrauna nustatymus is registro.
   ---------------------------------------------------------------------------------------------- */
void GetSettings(void){
    PHKEY hKey;
    DWORD dwDisposition, ret;

    /* Sukuria arba atidaro registro rakta */
    ret = RegCreateKeyEx(HKEY_CURRENT_USER, REGPATH, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_QUERY_VALUE, NULL, &hKey, &dwDisposition);

    /* Rasomi nustatymai */
    if((ret == ERROR_SUCCESS) && (dwDisposition != REG_CREATED_NEW_KEY)){
        DWORD dwSize;

        dwSize = sizeof(int);
        RegQueryValueEx(hKey, "Speed", NULL, NULL, &sets.speed, &dwSize);

        dwSize = sizeof(BOOL);
        RegQueryValueEx(hKey, "FullScr", NULL, NULL, &sets.fullscreen, &dwSize);
        RegQueryValueEx(hKey, "SndOn", NULL, NULL, &sets.sndon, &dwSize);
        RegQueryValueEx(hKey, "MusOn", NULL, NULL, &sets.muson, &dwSize);

        RegQueryValueEx(hKey, "CustMus", NULL, NULL, &sets.custommus, &dwSize);

        dwSize = MAX_PATH;
        RegQueryValueEx(hKey, "CustMusPath", NULL, NULL, sets.musfile, &dwSize);

        RegCloseKey (hKey);
    }

    /* Jeigu sukurtas naujas raktas arba atidaryti nepavyko, rasomi default'iniai nustatymai */
    else{
        sets.speed = 50;
        sets.fullscreen = TRUE;
        sets.sndon = TRUE;
        sets.muson = TRUE;
        sets.custommus = FALSE;
        sets.musfile[0] = 0;
    }

//!! Sugrizti, kai developinsiu installeri
    ret = RegOpenKeyEx(HKEY_LOCAL_MACHINE, REG_GLOBALSETS, 0, KEY_QUERY_VALUE | 0x0100, &hKey);
    if (ret == ERROR_SUCCESS){
        RegQueryValueEx(hKey, "GAME_PATH", NULL, NULL, REG_SZ, (PBYTE)sets.gamepath);
        RegCloseKey(hKey);
    }
    else strcpy(sets.gamepath, "%userprofile%");

}

/* ----------------------------------------------------------------------------------------------
    Funkcija  : SaveSettings()
    Aprasymas : Issaugo nustatymus i registra.
   ---------------------------------------------------------------------------------------------- */
void SaveSettings(void){
    PHKEY hKey;
    DWORD ret;

    ret = RegOpenKeyEx(HKEY_CURRENT_USER, REGPATH, 0, KEY_SET_VALUE, &hKey);

    if(ret == ERROR_SUCCESS){
        RegSetValueEx(hKey, "Speed", 0, REG_DWORD, (PBYTE)&sets.speed, sizeof(DWORD));

        RegSetValueEx(hKey, "FullScr", 0, REG_DWORD, (PBYTE)&sets.fullscreen, sizeof(DWORD));
        RegSetValueEx(hKey, "SndOn", 0, REG_DWORD, (PBYTE)&sets.sndon, sizeof(DWORD));
        RegSetValueEx(hKey, "MusOn", 0, REG_DWORD, (PBYTE)&sets.muson, sizeof(DWORD));

        RegSetValueEx(hKey, "CustMus", 0, REG_DWORD, (PBYTE)&sets.custommus, sizeof(DWORD));
        RegSetValueEx(hKey, "CustMusPath", 0, REG_SZ, (PBYTE)sets.musfile, strlen(sets.musfile));

        RegCloseKey(hKey);
    }
}
