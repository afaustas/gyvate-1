/* ===================================================================================
    GYVATE v1.0 (aprasyma ziureti main.c faile arba dokumentacijoje)
   ===================================================================================
    FAILAS: GAMETHREAD.C
    PASKIRTIS: Zaidimo threadas, jo funkcijos. Pauzes funkcija. Zaidimo grafines situa-
    cijos piesimo funkcijos, kurias naudoja thread'as, kad butu galima atvaizduoti zai-
    dima.
   -----------------------------------------------------------------------------------
    Kompiliatorius: gcc (TDM-2 mingw32) 4.4.1
    Faustas - Azuolas BAGDONAS, 2011-12
   ===================================================================================  */
#include "gamethread.h"

/* ----------------------------------------------------------------------------------------------
    Funkcija  : CreateGameThread()
    Argumentas: hDC - DC handleris. Cia paduodamas ne displejaus DC, bet buferinis DC (nes double buffering).
    Grazina   : Sukurto thread'o handleri.
    Aprasymas : Sukuria zaidimo threada, kuris ir valdo zaidima.
   ---------------------------------------------------------------------------------------------- */
HANDLE CreateGameThread(HDC hDC){
    static HANDLE hTread;
    WORD tID;
    DWORD dwTlsIndex;

    g_paused_game = FALSE;
    g_turn = NONE;
    score = 0;
    scoreinc = (sets.speed + 10) / 10; /* Tasku priklausomai nuo greicio didinimo formule. */

    /* Pries sukuriant threada paruosiamas screen[][] (zaidimo matrica) */
    if(!loadlevel(g_lvlpath)){
        error(IDS_ELVLLDERR, NULL, GetModuleHandle(NULL));
        return NULL;
    }

    load_snake();
    put_food();

    score = 0; /* ...ir init. score */

    /* O tada jau kuriamas thread */
    hTread = CreateThread(NULL, 0, GameThreadProc, hDC, CREATE_SUSPENDED, &tID);
    if(hTread == NULL)
        error(IDS_ETHREADC, NULL, GetModuleHandle(NULL));/* "Nepavyko sukurti thread" */

    return hTread;
}

/* ----------------------------------------------------------------------------------------------
    Funkcija  : GameThreadProc()
    Argumentas: hDC - DC handleris. Cia paduodamas ne displejaus DC, bet buferinis DC (nes double buffering).
    Grazina   : 0, kai zaidimas baigiamas.
    Aprasymas : Sioje proceduroje yra while() ciklas, kuriame vykdomas zaidimas iki mirties arba
    threado sustabdymo.
   ---------------------------------------------------------------------------------------------- */
DWORD WINAPI GameThreadProc(HDC hDC){
    int x = 0;
    unsigned int prevscore = 0;
    /* Cia ikraunami ivairus bitmapiniai resursai (gyvates dalys, maistas, sienos, background) */
    HBITMAP hdcBitmaps[8]; /* Cia saugau SelectObject grazintus bitmapus, kad veliau sugrusciau juos atgal ir sunaikinciau */
    HBITMAP hBmpHead = LoadImage(NULL, "res\\shead.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
    HBITMAP hBmpWall = LoadImage(NULL, "res\\wall.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
    HBITMAP hBmpFood = LoadImage(NULL, "res\\food.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
    HBITMAP hBmpBg = LoadImage(NULL, "res\\bg.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
    HBITMAP hBmpBody = LoadImage(NULL, "res\\sbody.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
    HBITMAP hBmpEnd = LoadImage(NULL, "res\\send.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
    HBITMAP hBmpTurn = LoadImage(NULL, "res\\sturn.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
    HDC hSheadDC = CreateCompatibleDC(hDC);
    HDC hWallDC = CreateCompatibleDC(hDC);
    HDC hFoodDC = CreateCompatibleDC(hDC);
    HDC hBgDC = CreateCompatibleDC(hDC);
    HDC hBodyDC = CreateCompatibleDC(hDC);
    HDC hEndDC = CreateCompatibleDC(hDC);
    HDC hTurnDC = CreateCompatibleDC(hDC);
    HDC hBkpDC = CreateCompatibleDC(hBgDC);
    HBITMAP hBkpBmp = CreateCompatibleBitmap(hBgDC, 10, 10);
    hdcBitmaps[0] = SelectObject (hSheadDC, (HGDIOBJ)hBmpHead);
    hdcBitmaps[1] = SelectObject (hWallDC, (HGDIOBJ)hBmpWall);
    hdcBitmaps[2] = SelectObject (hFoodDC, (HGDIOBJ)hBmpFood);
    hdcBitmaps[3] = SelectObject (hBgDC, (HGDIOBJ)hBmpBg);
    hdcBitmaps[4] = SelectObject (hEndDC, (HGDIOBJ)hBmpEnd);
    hdcBitmaps[5] = SelectObject (hBodyDC, (HGDIOBJ)hBmpBody);
    hdcBitmaps[6] = SelectObject (hBkpDC, (HGDIOBJ)hBkpBmp);
    hdcBitmaps[7] = SelectObject (hTurnDC, (HGDIOBJ)hBmpTurn);

    /* Sis kintamasis nurodo, kad pradetas zaidimas */
    g_started_game = TRUE;
    /* Score atnaujinimas. Nurodomas pradinis score (0, jei naujas zaidimas) */
    PaintOnScore(score, 0, GetDlgItem(g_hMainWnd, IDC_SCORE), NULL);

    /* Atidaromas garsas, kuris pasigirsta suvalgius maista */
    mciSendStringA("open res\\food.wav type waveaudio alias food", NULL, 0, NULL);

    /*  {  ---- Cia tik debuginimo tikslais.
        char err[100];
        DWORD derr;
        derr = mciSendStringA("open res\\food.wav type waveaudio alias food", err, 99, NULL);
        printf("Erroras mci: %s\n", err);
        mciGetErrorString(derr, err, 99);
        printf("Erroras mciRET: %s\n", err);
        GetCurrentDirectory(99, err);

        printf("pwd: %s\n", err);
    } */

    /*if (hBmpHead) printf("SHEAD.BMP Loaded\n");
      RepaintAll( hSheadDC,  hBodyDC,  hEndDC, hTurnDC, hFoodDC,  hWallDC, hBgDC, hDC); */

    /* Stai cia ir yra tas pagrindinis zaidimo ciklas, kuris vykdomas iki gyvates mirties arba
       threado stabdymo (tuomet nutraukiamas visas sios funckijos vykdymas. */
    while(upd_snake(g_turn)){
        Sleep(210 - sets.speed * 2); /* Greicio milisekundem skaiciavimas pagal nurodyta gyvates greiti. */
        RepaintAll(  hSheadDC , hBodyDC,  hEndDC, hTurnDC, hFoodDC,  hWallDC, hBgDC, hDC);

        /* Jei upd_snake() funkcijoje buvo pakeistas score, ji atnaujinu ir score control'e, isskleidziu garsa. */
        if (prevscore != score){
            PaintOnScore(score, 0, GetDlgItem(g_hMainWnd, IDC_SCORE), NULL);
            prevscore = score;
            /* Isskleidziu garsa, kad maistas paimtas */
            if(sets.sndon){
                mciSendStringA("seek food to start", NULL, 0, NULL);
                mciSendStringA("play food", NULL, 0, NULL);
            }
        }
    }

    /* Zaidimas baigesi, gyvate numire. Cia pranesu apie rezultata */
    {
        RECORD currec;
        currec = GetRecord(g_lvlpath); /* Gaunu informacija apie dabartini rekorda, kad galeciau lygint */
        if(currec.record < score){
            DialogBox(GetModuleHandle(NULL), MAKEINTRESOURCE(IDD_DIALOG5), NULL, RecordDlgProc);
        }
        else{
            char resstr[128], resstr_filled[135], caption [25] = "";
            if (!LoadString(GetModuleHandle(NULL), IDS_RESULTSTR, resstr, 128)){
                strcpy(resstr, "(def) Game over! Rezultatas: %d\nRekordas: %d");
            }
            LoadString(GetModuleHandle(NULL), IDS_RESULTCAPSTR, caption, 25);
            sprintf(resstr_filled, resstr, score, currec.record);
            MessageBoxA(NULL, resstr_filled, caption, MB_OK | MB_ICONINFORMATION | MB_SYSTEMMODAL);
        }
    }

    /* Nurodau, kad zaidimas baigesi */
    g_started_game = FALSE;
    /* Atlaisvinu atminti */
    SelectObject (hSheadDC, (HGDIOBJ)hdcBitmaps[0]);
    SelectObject (hWallDC, (HGDIOBJ)hdcBitmaps[1]);
    SelectObject (hFoodDC, (HGDIOBJ)hdcBitmaps[2]);
    SelectObject (hBgDC, (HGDIOBJ)hdcBitmaps[3]);
    SelectObject (hEndDC, (HGDIOBJ)hdcBitmaps[4]);
    SelectObject (hBodyDC, (HGDIOBJ)hdcBitmaps[5]);
    SelectObject (hBkpDC, (HGDIOBJ)hdcBitmaps[6]);
    SelectObject (hTurnDC, (HGDIOBJ)hdcBitmaps[7]);
    DeleteDC(hSheadDC);
    DeleteDC(hWallDC);
    DeleteDC(hFoodDC);
    DeleteDC(hBgDC);
    DeleteDC(hEndDC);
    DeleteDC(hBodyDC);
    DeleteDC(hBkpDC);
    DeleteDC(hTurnDC);
    DeleteObject(hBmpHead);
    DeleteObject(hBmpWall);
    DeleteObject(hBmpFood);
    DeleteObject(hBmpBg);
    DeleteObject(hBmpBody);
    DeleteObject(hBmpEnd);
    DeleteObject(hBkpBmp);
    DeleteObject(hBmpTurn);
    /* Uzdarau maisto suvalgymo garso faila */
    mciSendStringA("close food", NULL, 0, NULL);

    return 0;
}

/* Nebepamenu kam. Nenaudojama.
BOOL IsTurned(void){
    static int turn_old = RIGHT;

    if (turn_old != g_turn){
        turn_old = g_turn;
        return TRUE;
    }

    return FALSE;
}
*/

/* ----------------------------------------------------------------------------------------------
    Funkcija  : RepaintAll()
    Argumentai: Pirmi 7 hDC nurodo i ivairius DC, kuriuose uzloadinti ivairiu gyvates daliu, maisto,
                sienos, backgound'o bitmapai. Jie uzloadinami GameThreadProc() funkcijoje, is kurios
                si ir kvieciama. Paskutinis argumentas hMemDC nurodo i DC, kuriame bus grafiskai at-
                vaizduota zaidimo situacija.
    Aprasymas : Perpiesia situacija grafiskai i hMemDC.
   ---------------------------------------------------------------------------------------------- */
void RepaintAll(HDC hSheadDC, HDC hBodyDC, HDC hEndDC, HDC hTurnDC, HDC hFoodDC, HDC hWallDC, HDC hBgDC, HDC hMemDC){
    int rotate_deg, dalis;
    HDC hWndDC = GetDC(g_hMainWnd);
    HDC hDCMask;
    BOOL bKampas;
    COORDSN priekampo[3];

    BitBlt(hMemDC, 0, 0, 640, 455, hBgDC, 0, 0, SRCCOPY);

    /* Galvos apsukimas */
    switch(g_turn){
        case RIGHT:rotate_deg = 0; break;
        case UP  : rotate_deg = 90;  break;
        case DOWN: rotate_deg = 270; break;
        case LEFT: rotate_deg = 180; /* =0; StretchBlt(hSheadDC, 9, 0, -10, 10, hSheadDC, 0, 0, 10, 10, SRCCOPY)*/; break;
    }
    #ifdef DEBUG
    printf("\nRotate_deg = %u; g_turn: %u", rotate_deg, g_turn);
    #endif
    RotateDC(hSheadDC, 11, 11, rotate_deg);
    //BitBlt(hMemDC, snk.data[0].x * 10, snk.data[0].y * 10, 10, 10, hSheadDC, 0, 0, SRCCOPY);
    PutSnakePart(hMemDC, snk.data[0].x * 10, snk.data[0].y * 10, hSheadDC);
    RotateDC(hSheadDC, 11, 11, 360 - rotate_deg);
    //if (g_turn == LEFT) StretchBlt(hSheadDC, 9, 0, -10, 10, hSheadDC, 0, 0, 10, 10, SRCCOPY);
    /* Kuno apsukimas. x - gyvates dalis. */
    for(dalis = 1; dalis < snk.len - 1; dalis++){

       /* Gyvates dalis staciu kampu jeigu */
       priekampo[1] = snk.data[dalis];
       if (snk.data[dalis + 1].x > snk.data[dalis - 1].x) { priekampo[2] = snk.data[dalis + 1]; priekampo[0] = snk.data[dalis - 1];}
       else { priekampo[0] = snk.data[dalis + 1]; priekampo[2] = snk.data[dalis - 1];}

        if( priekampo[1].x ==  priekampo[2].x &&  priekampo[1].x !=  priekampo[0].x &&
            priekampo[1].y ==  priekampo[0].y &&  priekampo[1].y !=  priekampo[2] .y){
               ( priekampo[1].y >  priekampo[2].y) ? (rotate_deg = 180) : (rotate_deg = 270);
               bKampas = TRUE;
        }

        else if( priekampo[1].x ==  priekampo[0].x &&  priekampo[1].x !=  priekampo[2].x &&
            priekampo[1].y ==  priekampo[2].y &&  priekampo[1].y !=  priekampo[0] .y){
               ( priekampo[1].y <  priekampo[0].y) ? (rotate_deg = 0) : (rotate_deg = 90);
               bKampas = TRUE;
        }

        /* Jei ne kampas, verciu kuno dali */
        else{
            if((snk.data[dalis].x == snk.data[dalis - 1].x) || (snk.data[dalis].x == snk.data[dalis + 1].x)){
                rotate_deg = 90;
            }
            else if ((snk.data[dalis].y == snk.data[dalis - 1].y) || (snk.data[dalis].y == snk.data[dalis + 1].y)){
                rotate_deg = 0;
            }

            RotateDC(hBodyDC, 10, 10, rotate_deg);
            PutSnakePart(hMemDC, snk.data[dalis].x * 10, snk.data[dalis].y * 10, hBodyDC);
            RotateDC(hBodyDC, 10, 10, 360 - rotate_deg);
            bKampas = FALSE;
        }

        if(bKampas){
            RotateDC(hTurnDC, 11, 11, rotate_deg);
            PutSnakePart(hMemDC, snk.data[dalis].x * 10, snk.data[dalis].y * 10, hTurnDC);
            RotateDC(hTurnDC, 11, 11, 360 - rotate_deg);
        }
    }

    /* Uodegos sukimas */
    if(snk.data[snk.len - 1].y > snk.data[snk.len - 2].y)
        rotate_deg = 90;
    else if(snk.data[snk.len - 1].x > snk.data[snk.len - 2].x){
        StretchBlt(hEndDC, 9, 0, -10, 10, hEndDC, 0, 0, 10, 10, SRCCOPY);
        rotate_deg = 0;
    }
    else if(snk.data[snk.len - 1].x < snk.data[snk.len - 2].x)
        rotate_deg = 0;
    else if (snk.data[snk.len - 1].y < snk.data[snk.len - 2].y)
        rotate_deg = 270;

    RotateDC(hEndDC, 10, 10, rotate_deg);
    //BitBlt(hMemDC, snk.data[dalis].x * 10, snk.data[dalis].y * 10, 10, 10, hEndDC, 0, 0, SRCCOPY);
    PutSnakePart(hMemDC, snk.data[dalis].x * 10, snk.data[dalis].y * 10, hEndDC);
    RotateDC(hEndDC, 10, 10, 360 - rotate_deg);
    if (snk.data[snk.len - 1].x > snk.data[snk.len - 2].x) StretchBlt(hEndDC, 9, 0, -10, 10, hEndDC, 0, 0, 10, 10, SRCCOPY);

    /* Maisto rodymas */
    {
        int x, y;
        for(y = 0; y < 48; y++){
            for(x = 0; x < 64; x++){
                if(screen[y][x] == 'F')
                    //BitBlt(hMemDC, x * 10, y * 10, 10, 10, hFoodDC, 0, 0, SRCCOPY);
                    PutSnakePart(hMemDC, x * 10, y * 10, hFoodDC);
            }
       }

    }

    /* Sienos rodymas */
    {
        int x, y;
        for(y = 0; y < 48; y++){
            for(x = 0; x < 64; x++){
                if(screen[y][x] == '*')
                    BitBlt(hMemDC, x * 10, y * 10, 10, 10, hWallDC, 0, 0, SRCCOPY);
            }
       }
    }

    BitBlt(hWndDC, 0, 0, 640, 455, hMemDC , 0, 0, SRCCOPY );
    ReleaseDC(g_hMainWnd, hWndDC);
    DeleteDC(hDCMask);
}

/* ----------------------------------------------------------------------------------------------
    Funkcija  : PauseGame()
    Argumentai: hThread - zaidimo thread'o handleris.
                hWnd - lango handleris.
                hDC - DC, kuriame grafiskai saugoma informacija apie zaidimo busena (ten bus nupie-
                siamas zodis "Pauze". Iprastai tai atminties DC, o ne Screen DC, kadangi realizuotas
                double buffering siekiant padidinti greiti ir sutvarkyti lango perpiesimus.
    Aprasymas : Isveda uzrasa "Pauze" didelem melynom raidem i hDC.
   ---------------------------------------------------------------------------------------------- */
void PauseGame(HANDLE hThread, HWND hWnd, HDC hDC){
    RECT rect;
    HFONT hFont, hDefFont;
    char pausestr[16];
    //HDC hDC;
    /* static BOOL paused = FALSE; */

    //hDC = GetDC(hWnd);
    GetClientRect(hWnd, &rect);
    if (!g_paused_game){

        SuspendThread(hThread);
        //hDC = GetDC(hWnd);
        GetClientRect(hWnd, &rect);

        if (!LoadString(GetModuleHandle(NULL), IDS_PAUSESTR, pausestr, 16)){
            strcpy(pausestr, "Pause"); /* Jei nepavyko ikrauti, rasoma iprasta kalba */
        }

        /* Parenku fonta */
        hFont = CreateFont(100, /* Height, px*/
                        30, /* Width */
                        0, /* Escapement */
                        0, /* Orentation */
                        FW_BOLD, /* Ant kiek bold */
                        FALSE, /* Italic */
                        FALSE, /* Underline */
                        FALSE, /* No strikeout */
                        DEFAULT_CHARSET, /* Charset - pagal regional settings */
                        OUT_RASTER_PRECIS, /* Jei toks yra ir raster ir vector, renkuosi raster - lengviau renderint */
                        CLIP_DEFAULT_PRECIS,
                        DEFAULT_QUALITY, /* Priklausomai nuo vartotojo nustatymu */
                        FF_DONTCARE, //!!!!
                        "Courier New");

        hDefFont = SelectObject(hDC, hFont);

        /* O cia piesiu ant lango */
        SetTextColor(hDC, RGB(0,0,255));
        SetBkMode(hDC, TRANSPARENT);
        DrawText((HDC)hDC, pausestr, strlen(pausestr), &rect, DT_CENTER|DT_VCENTER|DT_SINGLELINE); /* Buvau ifu apskliaudes, taciau nebezinau kam */
        SelectObject(hDC, hDefFont);
        DeleteObject(hFont);
        g_paused_game = TRUE;

    }
    else{
        g_paused_game = FALSE;
        ResumeThread(hThread);
    }

    RefreshWindow(hWnd);
}

/* ----------------------------------------------------------------------------------------------
    Funkcija  : RotateDC()
    Argumentai: hDC - hDC, kuriame esanti bitmapa reikia pasukti
                Width - Bitmapo Width.
                Height - Bitmapo Height.
                angle - kokiu kampu pasukti.
    Return    : TRUE, jei pavyko pasukti.
    Aprasymas : hDC esanti bitmapa pasuka nurodytu kampu. Siekiant pagreitinti darba, nenaudojau
                bereikalingu skaiciavimu, pasistengiau isvengti cia nereikalingos trigonometrijos,
                darbo su slankiojo kablelio skaiciais ir palikau galimybe bitmapa pasukti tik 0,
                90, 180, 270 laipsniu kampais. Kolkas to pilnai uztenka siame zaidime.
   ---------------------------------------------------------------------------------------------- */
BOOL RotateDC(HDC hDC, int Width, int Height, int angle){
    HDC htmpDC;
    HBITMAP htmpBitmap, hBmpSiuksle;
    XFORM xform, xformold;
    DWORD goldmod;
    BOOL status = FALSE;

    if ((angle == 0) || (angle == 360))
        return TRUE;

    /* Sukuriu DC, suderinama su hDC ir ikraunu i ji bitmap'a */
    if((htmpDC = CreateCompatibleDC(hDC)) == NULL)
        return FALSE;
    if((htmpBitmap = CreateCompatibleBitmap(hDC, Width, Height))==NULL){
        DeleteDC(htmpDC);
        return FALSE;
    }
    hBmpSiuksle = SelectObject(htmpDC, htmpBitmap);

    /* Gaunu dabartini DC rezima. */
    goldmod = GetGraphicsMode(htmpDC);

    /* Nustatau GM_ADVANCED, kad galeciau naudoti transformacijas */
    SetGraphicsMode(htmpDC, GM_ADVANCED);

    /* Gaunu dabartine tmpDC transformaciju struktura */
    GetWorldTransform(htmpDC, &xformold);

    /* Uzpildau nauja transformaciju struktura pagal pasirinkimus */
    /* eMxx elementai turetu buti sin() ir cos(), bet tiesiogiai mat. f-ju nenaudoju
       noredamas isvengti bereikalingu skaiciavimu kompiuteriui ir papildomos
       bibliotekos (math.h) naudojimo, kuomet galima issiversti ir be jos.    */
    switch(angle){ /* SetWorldTransform() kampus skaiciuoja pagal laikrodzio rodykle, o as iprates pries. Todel pakeiciu case */
        case 270:
            xform.eM11= (float)0; //cos 90
            xform.eM12=(float)1; //sin 90
            xform.eM21=(float)-1; //-sin 90
            xform.eM22=(float)0; //cos 90
            break;
        case 180:
            xform.eM11= (float)-1; //cos 180
            xform.eM12=(float)0; //sin 180
            xform.eM21=(float)0; //-sin 180
            xform.eM22=(float)-1; //cos 180
            break;
        case 90:
            xform.eM11= (float)0; //cos 270
            xform.eM12=(float)-1; //sin 270
            xform.eM21=(float)1; //-sin 270
            xform.eM22=(float)0; //cos 270
    }

    /* Cia nustatau, kurios bitmapo vietos atzvilgiu sukti. Siuo atveju centro atzvilgiu.
       Isplaukia is siu formuliu:
       x' = x * eM11 + y * eM21 + eDx,
       y' = x * eM12 + y * eM22 + eDy.                             */
    xform.eDx = Width/2 - (Width/2) * xform.eM11 - (Height/2) * xform.eM21;
    xform.eDy = Height/2 - (Width/2) * xform.eM12 - (Height/2) * xform.eM22;

    /* Atlieku tmpDC transformacijas - apverciu jo grafini lauka */
    if(SetWorldTransform(htmpDC, &xform)){
        /* Keliu paveiksleli is argumento DC i htmpDC, kuris jau yra apverstas.
           Kadangi jo grafinis laukas apverstas, tai ir paveikslelis, atsidures
           jame, bus apverstas.                                                   */
        if (BitBlt(htmpDC, 0,0,Width,Height,hDC,0,0,SRCCOPY))
            status = TRUE;

        SetWorldTransform(htmpDC,&xformold);
        SetGraphicsMode(htmpDC,goldmod);

        if (status){
            if (!BitBlt(hDC, 0,0,Width,Height,htmpDC,0,0,SRCCOPY)){
                status = FALSE;
            }
        }
  }

  /* Apsivalymas */
  SelectObject(htmpDC, hBmpSiuksle);
  DeleteObject(htmpBitmap);
  DeleteDC(htmpDC);

  return status;
}

/* ----------------------------------------------------------------------------------------------
    Funkcija  : PutSnakePart()
    Argumentai: hdcMem - hDC, i kuri desiu gyvates dali is hdcPart. Cia saugoma zaidimo situacija.
                x, y - Kordinates visutioniojo kairiojo kampo dalies, kuria desiu i hdcMem DC.
                hdcPart - kopijuojamos dalies DC.
    Aprasymas : Nukopijuoja gyvates dali is hdcPart i hdcMem, ties koordinatem (x; y). Ruzavas
                background, esantis hdcPart DC padaromas tampa permatomu po kopijavimo.
   ---------------------------------------------------------------------------------------------- */
void PutSnakePart(HDC hdcMem, int x, int y, HDC hdcPart){
    HBITMAP hbmMem, hbmMask;
    HBITMAP hbmSiuksle;
    HDC hdcMask = CreateCompatibleDC(hdcPart);

    /* Sukuriu BW bitmapa maskei ir DC jai */
    hbmMask = CreateBitmap(10, 10, 1, 1, NULL);
    hbmSiuksle = SelectObject(hdcMask, hbmMask);

    /* Nustatau, kad permatomu daliu spalva yra ruzava (kadangi tokios slykscios spalvos tikrai niekad zaidime nenaudosiu) */
    SetBkColor(hdcPart, RGB(255,0,255));

    /* Darau maske */
    BitBlt(hdcMask, 0, 0, 10, 10, hdcPart, 0, 0, SRCCOPY);

    /* Taikau maske */
    BitBlt(hdcMem, x, y, 10, 10, hdcPart, 0, 0, SRCINVERT);
    BitBlt(hdcMem, x, y, 10, 10, hdcMask, 0, 0, SRCAND);
    BitBlt(hdcMem, x, y, 10, 10, hdcPart, 0, 0, SRCINVERT);

    /* Atlaisvinu atminti */
    SelectObject(hdcMask, hbmSiuksle);
    DeleteDC(hdcMask);
    DeleteObject(hbmMask);
}
