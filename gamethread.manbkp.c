#include "gamethread.h"

HANDLE CreateGameThread(HDC hDC){
    static HANDLE hTread;
    WORD tID;
    DWORD dwTlsIndex;

    loadlevel(level);
    load_snake();
    put_food();

    hTread = CreateThread(NULL, 0, GameThreadProc, hDC, CREATE_SUSPENDED, &tID);
    if(hTread == NULL)
        MessageBoxA(NULL, "Nepavyko sukurti thread", "ERR", MB_OK);

    return hTread;
}

DWORD WINAPI GameThreadProc(HDC hDC){
    int x = 0;
    unsigned int prevscore = 0;
    HBITMAP hdcBitmaps[7]; /* Cia saugau SelectObject grazintus bitmapus, kad veliau sugrusciau juos atgal ir sunaikinciau */
    HBITMAP hBmpHead = LoadImage(NULL, "res\\shead.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
    HBITMAP hBmpWall = LoadImage(NULL, "res\\wall.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
    HBITMAP hBmpFood = LoadImage(NULL, "res\\food.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
    HBITMAP hBmpBg = LoadImage(NULL, "res\\bg.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
    HBITMAP hBmpBody = LoadImage(NULL, "res\\sbody.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
    HBITMAP hBmpEnd = LoadImage(NULL, "res\\send.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
    HDC hSheadDC = CreateCompatibleDC(hDC);
    HDC hWallDC = CreateCompatibleDC(hDC);
    HDC hFoodDC = CreateCompatibleDC(hDC);
    HDC hBgDC = CreateCompatibleDC(hDC);
    HDC hBodyDC = CreateCompatibleDC(hDC);
    HDC hEndDC = CreateCompatibleDC(hDC);
    HDC hBkpDC = CreateCompatibleDC(hBgDC);
    HBITMAP hBkpBmp = CreateCompatibleBitmap(hBgDC, 10, 10);
    hdcBitmaps[0] = SelectObject (hSheadDC, (HGDIOBJ)hBmpHead);
    hdcBitmaps[1] = SelectObject (hWallDC, (HGDIOBJ)hBmpWall);
    hdcBitmaps[2] = SelectObject (hFoodDC, (HGDIOBJ)hBmpFood);
    hdcBitmaps[3] = SelectObject (hBgDC, (HGDIOBJ)hBmpBg);
    hdcBitmaps[4] = SelectObject (hEndDC, (HGDIOBJ)hBmpEnd);
    hdcBitmaps[5] = SelectObject (hBodyDC, (HGDIOBJ)hBmpBody);
    hdcBitmaps[6] = SelectObject(hBkpDC, hBkpBmp);

    if (hBmpHead) printf("SHEAD.BMP Loaded\n");

    RepaintAll( hSheadDC,  hBodyDC,  hEndDC, hFoodDC,  hWallDC, hBgDC, hDC); ///!!!
    while(upd_snake(g_turn)){
        Sleep(300);
        RepaintAll(  hSheadDC , hBodyDC,  hEndDC, hFoodDC,  hWallDC, hBgDC, hDC);

        if (prevscore != score){
            PaintOnScore(score, 0, GetDlgItem(g_hMainWnd, IDC_SCORE), NULL);
            prevscore = score;
            printf("\nScore atnajintas \n");
        }
    }

    {
        char resstr[40];
        sprintf(resstr, "Game over! Rezultatas: %d", score);
        MessageBoxA(NULL, resstr, "Game Over", MB_OK | MB_ICONINFORMATION);
    }

    SelectObject (hSheadDC, (HGDIOBJ)hdcBitmaps[0]);
    SelectObject (hWallDC, (HGDIOBJ)hdcBitmaps[1]);
    SelectObject (hFoodDC, (HGDIOBJ)hdcBitmaps[2]);
    SelectObject (hBgDC, (HGDIOBJ)hdcBitmaps[3]);
    SelectObject (hEndDC, (HGDIOBJ)hdcBitmaps[4]);
    SelectObject (hBodyDC, (HGDIOBJ)hdcBitmaps[5]);
    SelectObject (hBkpDC, (HGDIOBJ)hdcBitmaps[6]);
    DeleteDC(hSheadDC);
    DeleteDC(hWallDC);
    DeleteDC(hFoodDC);
    DeleteDC(hBgDC);
    DeleteDC(hEndDC);
    DeleteDC(hBodyDC);
    DeleteDC(hBkpDC);
    DeleteObject(hBmpHead);
    DeleteObject(hBmpWall);
    DeleteObject(hBmpFood);
    DeleteObject(hBmpBg);
    DeleteObject(hBmpBody);
    DeleteObject(hBmpEnd);
    DeleteObject(hBkpBmp);

    return 0;
}

BOOL IsTurned(void){
    static int turn_old = RIGHT;

    if (turn_old != g_turn){
        turn_old = g_turn;
        return TRUE;
    }

    return FALSE;
}

void RepaintAll(HDC hSheadDC, HDC hBodyDC, HDC hEndDC, HDC hFoodDC, HDC hWallDC, HDC hBgDC, HDC hMemDC){
    int rotate_deg, dalis;
    HDC hWndDC = GetDC(g_hMainWnd);
    HDC hDCMask;

    BitBlt(hMemDC, 0, 0, 640, 455, hBgDC, 0, 0, SRCCOPY);

    /* Galvos apsukimas */
    switch(g_turn){
        case RIGHT:rotate_deg = 0; break;
        case UP  : rotate_deg = 90;  break;
        case DOWN: rotate_deg = 270; break;
        case LEFT: rotate_deg = 0; StretchBlt(hSheadDC, 9, 0, -10, 10, hSheadDC, 0, 0, 10, 10, SRCCOPY); break;
    }
    printf("\nRotate_deg = %u; g_turn: %u", rotate_deg, g_turn);
    RotateDC(hSheadDC, 10, 10, rotate_deg);
    BitBlt(hMemDC, snk.data[0].x * 10, snk.data[0].y * 10, 10, 10, hSheadDC, 0, 0, SRCCOPY);
    RotateDC(hSheadDC, 10, 10, 360 - rotate_deg);
    if (g_turn == LEFT) StretchBlt(hSheadDC, 9, 0, -10, 10, hSheadDC, 0, 0, 10, 10, SRCCOPY);
    /* Kuno apsukimas. x - gyvates dalis. */
    for(dalis = 1; dalis < snk.len - 1; dalis++){
        if((snk.data[dalis].x == snk.data[dalis - 1].x) || (snk.data[dalis].x == snk.data[dalis + 1].x)){
            rotate_deg = 90;
        }
        else if ((snk.data[dalis].y == snk.data[dalis - 1].y) || (snk.data[dalis].y == snk.data[dalis + 1].y)){
            rotate_deg = 0;
        }

        RotateDC(hBodyDC, 10, 10, rotate_deg);
        /* Darau, kad butu permatomas */
        MakeTransparent(hBodyDC, RGB(255,0,255)); ///////////////////
        BitBlt(hMemDC, snk.data[dalis].x * 10, snk.data[dalis].y * 10, 10, 10, hBodyDC, 0, 0, SRCINVERT);
        PutSnakePart(hMemDC, snk.data[dalis].x * 10, snk.data[dalis].y * 10, hbmpBody);
        RotateDC(hBodyDC, 10, 10, 360 - rotate_deg);
    }

    /* Uodegos sukimas */
    if(snk.data[snk.len - 1].y > snk.data[snk.len - 2].y)
        rotate_deg = 90;
    else if(snk.data[snk.len - 1].x > snk.data[snk.len - 2].x){
        StretchBlt(hEndDC, 9, 0, -10, 10, hEndDC, 0, 0, 10, 10, SRCCOPY);
        rotate_deg = 0;
    }
    else if(snk.data[snk.len - 1].x < snk.data[snk.len - 2].x)
        rotate_deg = 0;
    else if (snk.data[snk.len - 1].y < snk.data[snk.len - 2].y)
        rotate_deg = 270;

    RotateDC(hEndDC, 10, 10, rotate_deg);
    BitBlt(hMemDC, snk.data[dalis].x * 10, snk.data[dalis].y * 10, 10, 10, hEndDC, 0, 0, SRCCOPY);
    RotateDC(hEndDC, 10, 10, 360 - rotate_deg);
    if (snk.data[snk.len - 1].x > snk.data[snk.len - 2].x) StretchBlt(hEndDC, 9, 0, -10, 10, hEndDC, 0, 0, 10, 10, SRCCOPY);

    /* Maisto rodymas */
    {
        int x, y;
        for(y = 0; y < 48; y++){
            for(x = 0; x < 64; x++){
                if(screen[y][x] == 'F')
                    BitBlt(hMemDC, x * 10, y * 10, 10, 10, hFoodDC, 0, 0, SRCCOPY);
            }
       }

    }

    /* Sienos rodymas */
    {
        int x, y;
        for(y = 0; y < 48; y++){
            for(x = 0; x < 64; x++){
                if(screen[y][x] == '*')
                    BitBlt(hMemDC, x * 10, y * 10, 10, 10, hWallDC, 0, 0, SRCCOPY);
            }
       }
    }
    //else
      //  BitBlt(hMemDC, uodegos_koord.x, uodegos_koord.y, 10, 10, hBkpDC, 0, 0, SRCCOPY);
    //int x, y;
   // int rotate_deg;
    //HDC hWndDC = GetDC(g_hMainWnd);
    /*for(y = 0; y < 48; y++){ //!!!
        for(x = 0; x < 64; x++){
            switch(screen[y][x]){
                case 'G':
                    {
                        //-----2011-10-25
                        switch(g_turn){
                            case UP  : rotate_deg = 90;  break;
                            case DOWN: rotate_deg = 270; break;
                            case LEFT: rotate_deg = 180; break;
                            case RIGHT:rotate_deg = 0;   break;
                        }
                        //BitBlt(hMemDC, x * 10, y * 10, 10, 10, hSheadDC, 0, 0, SRCCOPY);
                    }
                    break;
                case 'F': BitBlt(hMemDC, x * 10, y * 10, 10, 10, hFoodDC, 0, 0, SRCCOPY); break;
                case '*': BitBlt(hMemDC, x * 10, y * 10, 10, 10, hWallDC, 0, 0, SRCCOPY); break;
                case '.': BitBlt(hMemDC, x * 10, y * 10, 10, 10, hBgDC , 0, 0, SRCCOPY); break;
                //case '.':
            }
          /*  BitBlt(hWndDC, x * 10, y * 10, 10, 10, hMemDC , x * 10, y * 10, SRCCOPY ); - gabaliukais atnaujinimas pasirodo reikalauja daugiau resursu */
       // }
    // }
    BitBlt(hWndDC, 0, 0, 640, 455, hMemDC , 0, 0, SRCCOPY );
    ReleaseDC(g_hMainWnd, hWndDC);
    DeleteDC(hDCMask);
}

void PauseGame(HANDLE hThread, HWND hWnd, HDC hDC){
    RECT rect;
    HFONT hFont, hDefFont;
    char pausestr[16];
    //HDC hDC;
    static BOOL paused = FALSE;

    //hDC = GetDC(hWnd);
    GetClientRect(hWnd, &rect);
    if (!paused){

        SuspendThread(hThread);
        //hDC = GetDC(hWnd);
        GetClientRect(hWnd, &rect);

        if (!LoadString(GetModuleHandle(NULL), IDS_PAUSESTR, pausestr, 16)){
            strcpy(pausestr, "Pauze");
        }

        /* Parenku fonta */
        hFont = CreateFont(100, /* Height, px*/
                        30, /* Width */
                        0, /* Escapement */
                        0, /* Orentation */
                        FW_BOLD, /* Ant kiek bold */
                        FALSE, /* Italic */
                        FALSE, /* Underline */
                        FALSE, /* No strikeout */
                        DEFAULT_CHARSET, /* Charset - pagal regional settings */
                        OUT_RASTER_PRECIS, /* Jei toks yra ir raster ir vector, renkuosi raster - lengviau renderint */
                        CLIP_DEFAULT_PRECIS,
                        DEFAULT_QUALITY, /* Priklausomai nuo vartotojo nustatymu */
                        FF_SCRIPT, //!!!!
                        "Courier New");

        hDefFont = SelectObject(hDC, hFont);

        /* O cia piesiu ant lango */
        SetTextColor(hDC, RGB(0,0,255));
        SetBkMode(hDC, TRANSPARENT);
        if(DrawText((HDC)hDC, pausestr, strlen(pausestr), &rect, DT_CENTER|DT_VCENTER|DT_SINGLELINE))
        SelectObject(hDC, hDefFont);
        DeleteObject(hFont);
        paused = TRUE;
    }
    else{
        paused = FALSE;
        ResumeThread(hThread);
    }

    RefreshWindow(hWnd);
}

BOOL RotateDC(HDC hDC, int Width, int Height, int angle){
    HDC htmpDC;
    HBITMAP htmpBitmap, hBmpSiuksle;
    XFORM xform, xformold;
    DWORD goldmod;
    BOOL status = FALSE;

    if ((angle == 0) || (angle == 360))
        return TRUE;

    /* Sukuriu DC, suderinama su hDC ir ikraunu i ji bitmap'a */
    if((htmpDC = CreateCompatibleDC(hDC)) == NULL)
        return FALSE;
    if((htmpBitmap = CreateCompatibleBitmap(hDC, Width, Height))==NULL){
        DeleteDC(htmpDC);
        return FALSE;
    }
    hBmpSiuksle = SelectObject(htmpDC, htmpBitmap);

    /* Gaunu dabartini DC rezima. */
    goldmod = GetGraphicsMode(htmpDC);

    /* Nustatau GM_ADVANCED, kad galeciau naudoti transformacijas */
    SetGraphicsMode(htmpDC, GM_ADVANCED);

    /* Gaunu dabartine tmpDC transformaciju struktura */
    GetWorldTransform(htmpDC, &xformold);

    /* Uzpildau nauja transformaciju struktura pagal pasirinkimus */
    /* eMxx elementai turetu buti sin() ir cos(), bet tiesiogiai mat. f-ju nenaudoju
       noredamas isvengti bereikalingu skaiciavimu kompiuteriui ir papildomos
       bibliotekos (math.h) naudojimo, kuomet galima issiversti ir be jos.    */
    switch(angle){ /* SetWorldTransform() kampus skaiciuoja pagal laikrodzio rodykle, o as iprates pries. Todel pakeiciu case */
        case 270:
            xform.eM11= (float)0; //cos 90
            xform.eM12=(float)1; //sin 90
            xform.eM21=(float)-1; //-sin 90
            xform.eM22=(float)0; //cos 90
            break;
        case 180:
            xform.eM11= (float)-1; //cos 180
            xform.eM12=(float)0; //sin 180
            xform.eM21=(float)0; //-sin 180
            xform.eM22=(float)-1; //cos 180
            break;
        case 90:
            xform.eM11= (float)0; //cos 270
            xform.eM12=(float)-1; //sin 270
            xform.eM21=(float)1; //-sin 270
            xform.eM22=(float)0; //cos 270
    }

    /* Cia nustatau, kurios bitmapo vietos atzvilgiu sukti. Siuo atveju centro atzvilgiu.
       Isplaukia is siu formuliu:
       x' = x * eM11 + y * eM21 + eDx,
       y' = x * eM12 + y * eM22 + eDy.                             */
    xform.eDx = Width/2 - (Width/2) * xform.eM11 - (Height/2) * xform.eM21;
    xform.eDy = Height/2 - (Width/2) * xform.eM12 - (Height/2) * xform.eM22;

    /* Atlieku tmpDC transformacijas - apverciu jo grafini lauka */
    if(SetWorldTransform(htmpDC, &xform)){
        /* Keliu paveiksleli is argumento DC i htmpDC, kuris jau yra apverstas.
           Kadangi jo grafinis laukas apverstas, tai ir paveikslelis, atsidures
           jame, bus apverstas.                                                   */
        if (BitBlt(htmpDC, 0,0,Width,Height,hDC,0,0,SRCCOPY))
            status = TRUE;

        SetWorldTransform(htmpDC,&xformold);
        SetGraphicsMode(htmpDC,goldmod);

        if (status){
            if (!BitBlt(hDC, 0,0,Width,Height,htmpDC,0,0,SRCCOPY)){
                status = FALSE;
            }
        }
  }

  /* Apsivalymas */
  SelectObject(htmpDC, hBmpSiuksle);
  DeleteObject(htmpBitmap);
  DeleteDC(htmpDC);

  return status;
}

void MakeTransparent(HDC hDCColor, COLORREF crTransparent){
    HDC hdcMask;
    HBITMAP hbmMask, hbmColor, hbmTmp, hbmSiuksle;
    BITMAP bm;
        hbmTmp = CreateBitmap(1, 1, 1, 1, NULL);
    hbmColor = SelectObject(hDCColor, hbmTmp);


    hbmMask = CreateBitmapMask(hbmColor, RGB(255,0,255));
    SelectObject(hDCColor, hbmColor);

    BitBlt(hDCColor, 0, 0, 10, 10, hbmMask, 0, 0, SRCAND);

}

HBITMAP CreateBitmapMask(HBITMAP hbmColour, COLORREF crTransparent)
{
	HDC hdcMem, hdcMem2;
	HBITMAP hbmMask;
	BITMAP bm;

	GetObject(hbmColour, sizeof(BITMAP), &bm);
	hbmMask = CreateBitmap(bm.bmWidth, bm.bmHeight, 1, 1, NULL);

	hdcMem = CreateCompatibleDC(0);
	hdcMem2 = CreateCompatibleDC(0);

	SelectObject(hdcMem, hbmColour);
	SelectObject(hdcMem2, hbmMask);

	SetBkColor(hdcMem, crTransparent);

	BitBlt(hdcMem2, 0, 0, bm.bmWidth, bm.bmHeight, hdcMem, 0, 0, SRCCOPY);

	BitBlt(hdcMem, 0, 0, bm.bmWidth, bm.bmHeight, hdcMem2, 0, 0, SRCINVERT);

	DeleteDC(hdcMem);
	DeleteDC(hdcMem2);

	return hbmMask;
}
