/* ===================================================================================
    GYVATE v1.0 (aprasyma ziureti main.c faile arba dokumentacijoje)
   ===================================================================================
    FAILAS: SETTINGS.H
    PASKIRTIS: Nustatymu dialogas ir jo funkcijos. Issaugojimas ir atstatymas nustatymu
    is registro.
   -----------------------------------------------------------------------------------
    Kompiliatorius: gcc (TDM-2 mingw32) 4.4.1
    Faustas - Azuolas BAGDONAS, 2011-12
   ===================================================================================  */

#ifndef __SETTINGS_H
#define __SETTINGS_H

#include <windows.h>
#include <Windowsx.h>
#include <Commctrl.h>
#include <commdlg.h>
#include "resource.h"
#include "winreg.h"

#define REGPATH         "Software\\AFaustas\\Gyvate"    /* HKCU\%REGPATH%. Kur registre saugoti nustatymus. HKCU sakoj. */
#define REG_GLOBALSETS  "SOFTWARE\\AFaustas\\Gyvate"    //!!!

/* Nustatymu struktura */
typedef struct _settings{char gamepath[MAX_PATH]; /* Kelias iki zaidimo failu */
                         int speed; /* Gyvates greitis */
                         BOOL fullscreen, sndon, muson, custommus; /* custommus = TRUE, jei muzika nedefaultine */
                         char musfile[MAX_PATH];} SETTINGS; /* musfile - kelias iki muzikos failo (jei ne standartinis)  */

SETTINGS sets;

BOOL CALLBACK SettingsDlgProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
void SaveSettings(void);
void GetSettings(void);

#endif
