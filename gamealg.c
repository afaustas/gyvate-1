/* ===================================================================================
    GYVATE v1.0 (aprasyma ziureti main.c faile arba dokumentacijoje)
   ===================================================================================
    FAILAS: GAMEALG.H
    PASKIRTIS: Cia saugomas zaidimo algoritmas. Su WinAPI nesusijes, todel laisvai gali
    buti keliamas ant kitos platformos (portable).
   -----------------------------------------------------------------------------------
    Kompiliatorius: gcc (TDM-2 mingw32) 4.4.1
    Faustas - Azuolas BAGDONAS, 2011-12
   ===================================================================================  */
#include "gamealg.h"

/* ----------------------------------------------------------------------------------------------
    Funkcija  : loadlevel()
    Argumentai: *fname - lygio failo vardas (pilnas kelias)
    Grazina   : TRUE, jei pasiseke.
    Aprasymas : Ikrauna lygi is failo, kurio vardas %fname%, i screen[][] masyva.
   ---------------------------------------------------------------------------------------------- */
BOOL loadlevel(char *fname){
  /* Vars:
	 scr_pos_* - pozicija displejaus masyve
	 el_count - rodo i ta level masyvo elementa, su kuriuo dirbama
	 lap - pozicija level masyve (Level Array Pos)
	 flvl - lygio failo pointeris
  */
  FILE *flvl = fopen(fname, "rb");
  unsigned short int *level;
  unsigned short int scr_posx = 0, scr_posy = 0, el_count, lap;
  BOOL success = FALSE;

  if(flvl){
      unsigned short int testb = 0, size;
      fseek(flvl, 0, SEEK_END);
      size = ftell(flvl);
      fseek(flvl, 0, SEEK_SET);
      #ifdef DEBUG
      printf("\nFaila atidaryti pavyko\n");
      #endif

      if(size > 2) fread(&testb, sizeof(unsigned short int), 1, flvl);
      if(testb == 0x5947){ /* Tikrinamas Magic Number */
          #ifdef DEBUG
          printf("\nFailo testas pavyko\nFailo dydis: %d", size);
          #endif
          level = malloc(size + 2); /* Rezervuojama atmintis lygiui nuskaityti */
          if (level){
              #ifdef DEBUG
              printf("\nAtmintis rezervuota sekmingai");
              #endif
              fread(level, sizeof(unsigned short int), (size - 2)/sizeof(unsigned short int), flvl); /* Nuskaitomas faile gulintis lygis */
              #ifdef DEBUG
              printf("\nPo nuskaitymo");
              #endif
              /* Dekompresinamas lygis i screen[][]. Naudojamas pats primityviausias suspaudimas, koki tik galejau sugalvoti, zr. dokumentacija */
              for (lap = 0; lap < (size - 2)/sizeof(unsigned short int); lap++){
                for(el_count = 0; el_count < level[lap]; el_count++){
                  (lap % 2) ? (screen[scr_posy][scr_posx] = '*') : (screen[scr_posy][scr_posx] = '.');

                  scr_posx++;
                  if (scr_posx == 64){
                      scr_posx = 0;
                      scr_posy++;
                  }
                }
              }
              /* Atlaisvinu atminti, kadangi lygis jau dekompresintas i screen[][] */
              free(level);
              success = TRUE;
          }
      }
      fclose(flvl);
  }
  return success;
}

/* ----------------------------------------------------------------------------------------------
    Funkcija  : load_snake()
    Aprasymas : Ikrauna gyvate i screen[][] masyva ir initializuoja gyvates informacija.
   ---------------------------------------------------------------------------------------------- */
void load_snake(void){
  /* Gyvates galvos pozicija */
  snk.data[0].x=64/2;
  snk.data[0].y=48/2;
  /* Gyvates vieninteles vidurines kuno dalies pozicija */
  snk.data[1].y=snk.data[0].y;
  snk.data[1].x=snk.data[0].x - 1;
  /* Gyvates uodegos pozicija */
  snk.data[2].y=snk.data[0].y;
  snk.data[2].x=snk.data[0].x - 2;
  snk.len=3; /* Gyvates ilgis - 3 dalys. */

  /* Cia gyvate ikraunama i screen[][] masyva */
  screen[snk.data[0].y][snk.data[0].x] = 'G';
  screen[snk.data[0].y][snk.data[0].x - 1] = 'G';
  screen[snk.data[0].y][snk.data[0].x - 2] = 'G';
}

/* ----------------------------------------------------------------------------------------------
    Funkcija  : upd_snake()
    Argumentai: turn  - posukis. Viena is LEFT, RIGHT, UP, DOWN, NONE reiksmiu.
    Grazina   : TRUE, jei po veiksmo gyvate dar gyva.
    Aprasymas : Perstumia gyvate per viena pozicija pasirinkta kryptimi.
   ---------------------------------------------------------------------------------------------- */
BOOL upd_snake(int turn){
  unsigned int x, y, snk_pos;
  BOOL success = TRUE;
  x = snk.data[0].x;
  y = snk.data[0].y;

  /* Kaip pasikeis galvos koordinates po jos pajudinimo pasirinkta kryptimi */
  switch(turn){
	case UP:
	  if (y == 0) y = 45; /* Pakeista is 47 */
	  else y--;
	  break;
	case DOWN:
	  if (y == 45) y = 0; /* Pakeista is 47 */
	  else y++;
	  break;
	case LEFT:
	  if (x == 0) x = 63;
	  else x--;
	  break;
	case RIGHT:
	  if (x == 63) x = 0;
	  else x++;
	  break;
    case NONE:
      return TRUE;
  }

  /* Jei gyvate atsitrenke i siena (zymima '*') arba pati i save ('G'), tai mirtis ;-( */
  if (screen[y][x] == '*' || screen[y][x] == 'G'){
	success = FALSE;
  }

  /* O jeigu gyvate suvalge maista, tai labai gerai - didinamas rezultatas :-) */
  else if(screen[y][x] == 'F'){
	score += scoreinc;

    /* Suvalgius maista gyvate paauga */
	for(snk_pos = snk.len; snk_pos; snk_pos--){
	  snk.data[snk_pos] = snk.data[snk_pos - 1];
	}
	snk.data[0].x = x;
	snk.data[0].y = y;
	snk.len++;

	screen[y][x] = 'G';

	put_food();
  }

  /* Jeigu iki sio if'o gyvate dar nenumire, tai ji tiesiog pajuda viena pozicija i prieki pasirinkta kryptimi */
  else if (success){
	screen[snk.data[snk.len-1].y][snk.data[snk.len-1].x] = '.'; /* Pabaigoje tuscia vieta (nes gyvate nepailgejo) */
	/* Paskutine koordinate ismetu, o i pradzia irasau nauja galvos koordinate */
	for(snk_pos = snk.len - 1; snk_pos; snk_pos--){
	  snk.data[snk_pos] = snk.data[snk_pos - 1];
	}

	snk.data[0].x = x;
	snk.data[0].y = y;

	/* Gyvates perrasymas screen masyve */
	for (snk_pos = 0; snk_pos < snk.len; snk_pos++){
	  screen[snk.data[snk_pos].y][snk.data[snk_pos].x] = 'G';
	}
  }

  return success;
}

/* ----------------------------------------------------------------------------------------------
    Funkcija  : put_food()
    Aprasymas : Randomiskai padeda maista i.
   ---------------------------------------------------------------------------------------------- */
void put_food(void){
  unsigned int x;
  unsigned int y;

  do{
	x = rand() % 64;
	y = rand() % 45; /* Pakeista, kadangi displejaus apacia paaskirta informacijai */
  }
  while(screen[y][x] != '.');

  screen[y][x] = 'F';
}

/* ----------------------------------------------------------------------------------------------
    Funkcija  : save_game()
    Argumentai: *levelpath  - pilnas kelias iki lygio failo (iskaitant faailo varda)
    Grazina   : TRUE, jei pavyko issaugoti
    Aprasymas : Zaidimo issaugijimas
   ---------------------------------------------------------------------------------------------- */
BOOL save_game(char *levelpath){
    FILE *f;
    BOOL success = FALSE;
    char path[256];

    #ifdef DEBUG
    printf("\nSaugojimo f()\n");
    #endif
    strcpy(path, getenv(SYSFOLD)); /* Gaunama %APPDATA% direktorija */
    strcat(path, FNAME); /* Irasomas failo, i kuri bus saugomas zaidimas, pavadinimas */
    #ifdef DEBUG
    printf("path: %s\n",path);
    #endif
    /* Atidaromas failas, i kuri bus saveinamas zaidimas */
    if(f = fopen(path, "w+b")){
        #ifdef DEBUG
        printf("\nAtidarymas sekmingas\n");
        #endif
        if(fwrite(screen, sizeof(char), 64*48, f) == 64*48){ /* Surasomas screen[][] masyvas - esme */
            #ifdef DEBUG
            printf("\Screen rasymas sekmingas\n");
            #endif
            if(fwrite(&snk.len, sizeof(unsigned int), 1, f) == 1){ /* Rasomas gyvates ilgis */
                int x, written;
                #ifdef DEBUG
                printf("\Gyvates ilgio rasymas sekmingas\n");
                #endif
                for(x = 0, written = 0; x < snk.len; x++){
                    written += fwrite(&snk.data[x], sizeof(COORDSN), 1, f); /* Rasomos gyvates daliu koordinates */
                }
                #ifdef DEBUG
                printf("\Irasyta gyvates daliu: %d\n", written);
                #endif
                if(written == snk.len){
                    unsigned char len = strlen(levelpath);
                    fwrite(&len, sizeof(unsigned char), 1, f); /* Rasomas lygio failo vardo (f-jos argumento) ilgis */
                    fwrite(levelpath, 1, len, f); /* Rasomas kelias iki lygio failo (f-jos argumentas) */
                    fwrite(&score, sizeof(unsigned short int), 1, f); /* Irasomas dabartinis rezultatas */
                    success = TRUE; /* Kad sekmingai issaugotas zaidimas */
                }
            }
        }
        fclose(f);
    }
    if (!success)
        remove(path); /* Jeigu nepavyko issaugoti zaidimo - ismesti galbut nedarasyta faila. Nesiukslinti kad. */
    return success;
}

/* ----------------------------------------------------------------------------------------------
    Funkcija  : restore_game()
    Argumentai: *levelpath  - pilnas kelias iki lygio failo (iskaitant faailo varda)
    Grazina   : TRUE, jei pavyko atstatyti
    Aprasymas : Zaidimo atstatymas
   ---------------------------------------------------------------------------------------------- */
BOOL restore_game(char *levelpath){
    FILE *f;
    BOOL success = FALSE;
    char path[256];

    strcpy(path, getenv(SYSFOLD)); /* Gaunama %APPDATA% direktorija */
    strcat(path, FNAME); /* Irasomas failo, is kurio bus atstatomas zaidimas, pavadinimas */
    #ifdef DEBUG
    printf("path rest: %s\n",path);
    #endif

    /* Atidaromas failas, is kurio bus atstatomas zaidimas */
    if(f = fopen(path, "rb")){
        int x;
        fread(screen, sizeof(char), 64*48, f); /* Pildomas screen[][] masyvas */
        fread(&snk.len, sizeof(unsigned int), 1, f); /* Pildomas gyvates ilgis  gyvates informacijos strukturoje*/
        for (x = 0; x < snk.len; x++){
            fread(&snk.data[x], sizeof(COORDSN), 1, f); /* Gyvates daliu koordinates nuskaitomos */
        }
        /* Nuskaitomas kelias iki lygio failo. Pildomas argumentas */
        {
            unsigned char len;
            fread(&len, sizeof(unsigned char), 1, f);
            fread(levelpath, 1, len, f);
        }
        fread(&score, sizeof(unsigned short int), 1, f); /* Nuskaitomas dabartinis rezultatas */
        #ifdef DEBUG
        printf("Atstatyti taskai: %d\n", score);
        #endif
        success = TRUE;
        fclose(f);
        remove(path); /* Ismetamas failas, kuriame buvo issaugotas zaidimas, kadangi pavyko ji atstatyti */
    }
    return success;
}

/* ----------------------------------------------------------------------------------------------
    Funkcija  : saved_game_exist()
    Grazina   : TRUE, jei egzistuoja
    Aprasymas : Paprasta funkcija, patikrinanti ar yra issaugotas zaidimas (ar egzistuoja GYVATE.SAV)
   ---------------------------------------------------------------------------------------------- */
BOOL saved_game_exist(void){
    FILE *f;
    char path[256];

    strcpy(path, getenv(SYSFOLD));
    strcat(path, FNAME);
    #ifdef DEBUG
    printf("path rest: %s\n",path);
    #endif

    if(f = fopen(path, "r")){
        fclose(f);
        return TRUE;
    }
    return FALSE;
}
