/* ===================================================================================
    GYVATE v1.0
   ===================================================================================
    PROGRAMOS APRASYMAS:

    Tai paprasto ir visiems gerai visiems zinomo gyvates zaidimo realizacija su WinAPI.
    Detaliau - dokumentacijoje.
   -----------------------------------------------------------------------------------
    FAILAS: MAIN.C
    PASKIRTIS: Atsakingas uz UI, pagrindinio lango sukurima, piesima jame, meniu, etc.
   -----------------------------------------------------------------------------------
    Kompiliatorius: gcc (TDM-2 mingw32) 4.4.1
    Faustas - Azuolas BAGDONAS, 2011-12
   ===================================================================================  */
#include "main.h"

/* Pagrindine main() funkcija. Naudojant WinAPI vadinama truputi kitaip */
int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszArgument, int nCmdShow){
    MSG Msg;
    HBITMAP hBmpBg;
    HBRUSH hBrBg;
    HWND hwndPrev;

    /* Tikrinu: gal programa jau yra paleista? */
    if ((hwndPrev = FindWindow (g_szClassName, NULL)) != NULL){
        SetForegroundWindow (hwndPrev);
        return -1;
    }

    /* Del problemu su kompiliatorium sito teks kolkas nenaudoti - veikia ir be to
    {
        INITCOMMONCONTROLSEX icex;
        icex.dwICC = ICC_BAR_CLASSES|ICC_STANDARD_CLASSES|ICC_TAB_CLASSES;
        icex.dwSize = sizeof(INITCOMMONCONTROLSEX);
        if(!InitCommonControlsEx(0)){ /* Ikrauna COMDLG32.DLL, paruosia naudojimui reikiamus control *
            error(IDS_ECANTCOMM, NULL, hInstance);  //E:"Negalima inicializuoti common controls! Pabaiga."
            return 1;
        } //NELINKINA1!!
    } */

    /* Lango background brush ikrovimas is bitmapo */
    hBmpBg = (HBITMAP)LoadImage(NULL, "res\\mbg.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
    hBrBg = CreatePatternBrush(hBmpBg);
    if ((hBrBg == NULL) || (hBmpBg == NULL)){
        error(IDS_EBGBRUSH, NULL, hInstance);
    }

    /* Init: Gaunami zaidimo nustatymai, paleidziama muzika, parenkamas iprastinis lygis, randimize'inama */
    GetSettings();
    Music();
    strcpy(g_lvlpath, "levels\\default.glv");

    srand(time(NULL));
    g_turn = RIGHT; //Init.
    score = 0; //init
    g_started_game = FALSE;

    /* Jei fulscreen... */
    if(sets.fullscreen){
        /* ...Nustatomas FullScreen 640x480 rezoliucija */
        if(!set_fullscreen_resolution()){
            error(IDS_ENOFULLSCREEN, NULL, hInstance);
            sets.fullscreen = FALSE;
        }
    }

    if (!InitScoreCtrl()){
        error(IDS_ENOSCOREW, NULL, hInstance); //E: ("\nNepavyko score lauko paruosti!!!!\n");
    }

    /* Sukuriamas pagrindinis zaidimo langas */
    g_hMainWnd = create_main_window(hInstance, hPrevInstance, nCmdShow, &Msg, hBrBg, sets.fullscreen);

    if(!g_hMainWnd){
        error(IDS_ECMAINWND, NULL, hInstance);
        return 1;
    }

    /* Message Loop. Vykdomas, kol GetMessage() negrazina 0, t.y. vartotojas neuzdaro lango */
    while (GetMessage (&Msg, NULL, 0, 0))
    {
        /* Vercia virtual-key pranesimus, kad programa galetu reaguoti i klaviatura */
        TranslateMessage(&Msg);
        /* Pranesimai siunciami i WndProc */
        DispatchMessage(&Msg);
    }

    /* Atstatau iprastine rezoliucija */
    ChangeDisplaySettings(NULL, 0);

    /* Atlaisvinu atminti */
    DeleteObject(hBrBg);
    DeleteObject(hBmpBg);

    SaveSettings();

    /* Uzdarau muzikos ir garsu failus */
    mciSendStringA("close music", NULL, 0, NULL);
    mciSendStringA("close food", NULL, 0, NULL);
    mciSendStringA("close record", NULL, 0, NULL);

    /* Cia grazinama ta reiksme, kuri buvo duota PostQuitMessage() funkcijoje. Iprastai 0 */
    return Msg.wParam;
}

/* Pranesimo apie klaida parodymo funkcija */
void error(int err_res, HWND hWnd, HINSTANCE hInstance){
    char caption[64];
    char more_info[64];
    DWORD dwError = GetLastError();

    LoadString(hInstance, IDS_ECAPTION, caption, 63); /* Caption */
    g_szGenStr[0] = 0;

    if(err_res){
        LoadString(hInstance, IDS_EMOREINF, more_info, 63);  /* Po klaidos pranesimo zodziai "daugiau info"*/
        LoadString(hInstance, err_res, g_szGenStr, 2047); /* Klaidos aprasymas */
        if(err_res < 2000) goto ERROR_END; /* Cia parasyta, kad jei resurso ID mazesnis uz 2000, tai nereikia OS teikiamos info apie klaida. */
        wsprintf(&g_szGenStr[strlen(g_szGenStr)], "\n%s0x%0.8X: ", more_info, dwError);
        /* strcat(&g_szGenStr[strlen(g_szGenStr)], more_info); */
    }
    else {
        wsprintf(&g_szGenStr[strlen(g_szGenStr)], "0x%0.8X: ", dwError); /* Jei resurso ID nenurodytas, tik OS informacija */
    }
    /* OS klaidos stringas is errorcode */
    FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM|FORMAT_MESSAGE_IGNORE_INSERTS,
                 NULL,
                 dwError,
                 0,
                 &g_szGenStr[strlen(g_szGenStr)],
                 2047 - strlen(g_szGenStr),
                 NULL);
ERROR_END:
    MessageBoxA(hWnd, g_szGenStr, caption, MB_OK | MB_ICONERROR);
}

/* Pagrindine lango funkcija. Window Procedure */
LRESULT CALLBACK WndProc (HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam){
    static HANDLE hThread;
    static HBRUSH hbrBkgnd;

    static HDC hdcBuffer;
    static HBITMAP hbmBuffer;
    static HBITMAP hbmOldBuffer;
    static BOOL bMenu;

    switch (uMsg)                  /* handle the messages */
    {
        case WM_QUIT:
            break;
        case WM_CLOSE:
            /* Jei zaidimas pradetas, ji issaugau */
            if(g_started_game){
                if(!save_game(g_lvlpath)){
                    error(IDS_ECANTSAVE, hWnd, GetModuleHandle(NULL));
                }
            }
            /* Sunaikinu langa */
            DestroyWindow(hWnd);
            break;
        case WM_DESTROY:
            /* Nusiunciam WM_QUIT pranesima i pranesimu eile (angl. message queue) */
            PostQuitMessage (0);
            break;
        case WM_CREATE:
            {
                hStatusWnd = create_status(hWnd);
                HDC hMainWndDC = GetDC(hWnd);

                /* Sukuriu buferio DC, kadangi geriau pradzioje paisyti atmintyje, o tada kopijuoti i displeju */
                hdcBuffer = CreateCompatibleDC(hMainWndDC);
                hbmBuffer = CreateCompatibleBitmap(hMainWndDC, 640, 455);
                hbmOldBuffer = SelectObject(hdcBuffer, hbmBuffer);

                ReleaseDC(hWnd, hMainWndDC);

                /* Atstatau zaidima, jei yra issaugotas */
                if(saved_game_exist()){
                    TerminateThread(hThread, 0);
                    hThread = CreateGameThread(hdcBuffer);
                    if(hThread == NULL){
                        error(IDS_ETHREADC, hWnd, GetModuleHandle(NULL));/* "Nepavyko sukurti thread" */
                    }
                    if(!restore_game(g_lvlpath)){
                        error(IDS_ECANTSAVE, hWnd, GetModuleHandle(NULL));
                    }
                    else{
                        g_started_game = TRUE;
                        ResumeThread(hThread);
                    }
                }
                SetActiveWindow(hWnd);
            }
            break;
        case WM_ACTIVATEAPP:
                /* Iskvieiu meniu, kai aktyvuojamas langas */
                if(!bMenu){
                    SetForegroundWindow (hWnd);
                    Sleep(100);
                    keybd_event('M', 0xB2, 0, 0);
                    Sleep(100);
                    keybd_event('M', 0xB2, KEYEVENTF_KEYUP, 0);
                }
                break;
        case WM_PAINT:
            {
                PAINTSTRUCT ps;
                HDC hdcMainWnd = BeginPaint(hWnd, &ps);
                DWORD thread_exitcode;

                if(hdcMainWnd == NULL){
                    error(IDS_ERRDC, hWnd, GetModuleHandle(NULL));
                }

                if(!bMenu)
                    BitBlt(hdcMainWnd, 0, 0, 640, 455, hdcBuffer, 0, 0, SRCCOPY);

                /* ISJUNGTA */
                /* Jei zaidimas prasidejes, tai vaizda reik kopijuoti is buferio */
                /*if(!ResumeThread(hThread)) /* Jei threadas jau dirbo *
                    BitBlt(hdcMainWnd, 0, 0, 640, 455, hdcBuffer, 0, 0, SRCCOPY);
                else /* Jei jis nedirbo, tai tikrinimo metu as ji paleidau. Turiu vel sustabdyt. *
                    SuspendThread(hThread); */
                /* ENDOF ISJUNGTA */

                EndPaint(hWnd, &ps);
                #ifdef DEBUG
                printf("Perpiesiamas pgr. langas\n");
                #endif
            }
            break;
        case WM_KEYDOWN: /* Cia lango pranesimas, kai nuspaudziamas klaviaturos mygtukas */
            {
                switch(wParam){
                    case VK_UP   : g_turn = UP   ; break;
                    case VK_DOWN : g_turn = DOWN ; break;
                    case VK_LEFT : g_turn = LEFT ; break;
                    case VK_RIGHT: g_turn = RIGHT; break;
                    case 'P':
                    case 'p': PauseGame(hThread, hWnd, hdcBuffer); break; /* Pauze */
                    case 'm':
                    case 'M': /* Meniu */
                        {
                            int choice;
                            bMenu = TRUE;
                            /* Jei palikciau pauze ir kviesiciau meniu, veliau butu problemos norint pratesti */
                            if(g_paused_game) {
                                PauseGame(hThread, hWnd, hdcBuffer);
                                g_paused_game = FALSE;
                            }
                            SuspendThread(hThread);
                            RefreshWindow(hWnd);
                            choice = MainMenu(hWnd);
                            /* printf("\nPasirinkta: %u\n", choice); */
                            switch(choice){
                                case IDC_NEWCLASSIC: /* Pasirinktas naujas zaidimas */
                                    bMenu = FALSE;
                                    /* Kuriu threada, kuris valdys zaidima */
                                    TerminateThread(hThread, 0);
                                    hThread = CreateGameThread(hdcBuffer);
                                    if(hThread == NULL){
                                        error(IDS_ETHREADC, hWnd, GetModuleHandle(NULL));
                                        /* MessageBoxA(NULL, "Nepavyko sukurti thread ret", "ERR", MB_OK); */
                                    }
                                    ResumeThread(hThread);
                                    break;
                                case IDC_CONT: /* Pasirinkta testi pradeta zaidima */
                                    bMenu = FALSE;
                                    ResumeThread(hThread);
                                    break;
                                case IDC_EXIT: /* Nuspresta baigti zaidima, iseiti i OS */
                                    SendMessage(hWnd, WM_CLOSE, 0, 0);
                                    break;
                            }
                            ShowWindow(hStatusWnd, SW_SHOW);
                        }
                        break;
                }
            }
            break;

        default:
           /* Jei gaunu pranesima, kuriam specifiniu poreikiu neturiu,
              perduodu i iprastiniam vykdymui                          */
            return DefWindowProc (hWnd, uMsg, wParam, lParam);
    }

    return 0;
}

/* ----------------------------------------------------------------------------------------------
    Funkcija  : create_main_window()
    Argumentai: hInstance - programos instancija (kur uzloadinta)
                hPrevInstance
                nCmdShow - kaip paleista (su LNK failais pravercia iprastai)
                *Mgs - kur saugoti pranesimus
                hBrBg - background brushas
                bFullScreen - ar per visa displeju
    Grazina   : Sukurto lango handleri, jei pasiseke. Kitaip NULL.
    Aprasymas : Sukuria pagrindini programos langa ir grazina jo handleri.
   ---------------------------------------------------------------------------------------------- */
HWND create_main_window(HINSTANCE hInstance, HINSTANCE hPrevInstance, int nCmdShow, MSG *Mgs, HBRUSH hBrBg, BOOL bFullScreen){
    HWND hWnd;             /* Lango handleris */
    WNDCLASSEX wce;        /* Lango klases struktura */
    char caption[40];

    /* Uzpildoma lango struktura (klase) */
    wce.hInstance = hInstance;
    wce.lpszClassName = g_szClassName;
    wce.lpfnWndProc = WndProc; /* Nurodau Window Procedure, kuria kvies langa gaves pranesima */
    wce.style = CS_OWNDC | CS_VREDRAW | CS_HREDRAW;    /* Perpiesiamas */
    wce.cbSize = sizeof (WNDCLASSEX);

    wce.hIcon = LoadIcon (NULL, IDI_ICON1);
    wce.hIconSm = LoadIcon (NULL, IDI_ICON1);
    wce.hCursor =  LoadCursor (NULL, IDC_ARROW);
    wce.lpszMenuName = NULL;
    wce.cbClsExtra = 0;
    wce.cbWndExtra = 0;
    /* ... */
    wce.hbrBackground = hBrBg;

    /* Klases registracija */
    if (!RegisterClassEx (&wce))
        return NULL;

    if(!LoadString(hInstance, IDS_MAINCAPTION, caption, 40)){
        strcpy(caption, "Gyvate");
    }

    /* Klase uzregistruota, kuriamas langas */
    hWnd = CreateWindowEx(
           bFullScreen ? 0 : WS_EX_CLIENTEDGE,
           g_szClassName,         /* Lango klases vardas */
           caption,       /* Caption */
           WS_VISIBLE | (bFullScreen ? WS_POPUP : WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX), /* Langas matomas, per visa ekrana */
           (bFullScreen ? 0 : (CW_USEDEFAULT)),       /* Horizontali lango pozicija*/
           (bFullScreen ? 0 : (CW_USEDEFAULT)),       /* Vertikali lango pozicija */
           (bFullScreen ? 640 : 640 + 2*GetSystemMetrics(SM_CXEDGE)),                 /* Width */
           (bFullScreen ? 480 : 480 + GetSystemMetrics(SM_CYEDGE)*2+GetSystemMetrics(SM_CYFIXEDFRAME)+GetSystemMetrics(SM_CYCAPTION)),  /* Height, jei lange, o ne FullScr, tai dar caption ir kt. iskaicuoti */
           HWND_DESKTOP,        /* Desktop lango vaikas yra sis langas, t.y. hWndParent = 0 */
           NULL,                /* Nera meniu virsuje*/
           hInstance,           /* Programos instancija */
           NULL                 /* Nera papildomu duomenu langui*/
           );

    /* Parodyti langa */
    if(!ShowWindow (hWnd, nCmdShow)){
        PostQuitMessage(1);
        return 0;
    }

    return hWnd;
}

/* ----------------------------------------------------------------------------------------------
    Funkcija  : set_fullscreen_resolution()
    Grazina   : Jei pasiseke - TRUE. Kitaip FALSE.
    Aprasymas : Nustato VGA rezoliucija, langas per visa ekrana.
   ---------------------------------------------------------------------------------------------- */
BOOL set_fullscreen_resolution(void){
    DEVMODE dm;
    ZeroMemory(&dm, sizeof(DEVMODE));
    dm.dmSize = sizeof(DEVMODE);
    dm.dmBitsPerPel = 16;
    dm.dmPelsWidth = 640;
    dm.dmPelsHeight = 480;
    dm.dmFields = DM_PELSWIDTH | DM_PELSHEIGHT | DM_BITSPERPEL;

    if(ChangeDisplaySettings(&dm, CDS_FULLSCREEN) != DISP_CHANGE_SUCCESSFUL)
      return FALSE;

    return TRUE;
}

/* =======================================[ Pagrindinis Meniu ]============================================== */

/* ----------------------------------------------------------------------------------------------
    Funkcija  : MainMenu()
    Argumentai: hParent - tevinis langas
    Grazina   : Pasirinkimo ID
    Aprasymas : Parodomas pagrindinis meniu.
   ---------------------------------------------------------------------------------------------- */
int MainMenu(HWND hParent){
    MSG Msg;
    HWND hMainDlg;
    hMainDlg = CreateDialog(GetModuleHandle(NULL), MAKEINTRESOURCE(IDD_DIALOG1), hParent, MMDlgProc);

    if(hMainDlg == NULL){
        error(IDS_EMMENUC, hParent, GetModuleHandle(NULL));
        return -1; /* Tokiu atveju programa reiketu nutraukti */
    }

    /* Meniu dialogo Message Loop'as */
    while(GetMessage(&Msg, NULL, 0, 0) > 0)
    {
        if(!IsDialogMessage(hMainDlg, &Msg))
        {
            TranslateMessage(&Msg);
            DispatchMessage(&Msg);
        }
    }

    return Msg.wParam;
}

/* Meniu dialogo procedura */
BOOL CALLBACK MMDlgProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam){
    switch(msg)
    {
        case WM_INITDIALOG:
                /* Jei zaidimas neprasidejes, "Testi" mygtukas turi buti neaktyvus. */
                if(!g_started_game){
                    EnableWindow(GetDlgItem(hwnd, MAKEINTRESOURCE(IDC_CONT)), FALSE);
                }
            break;
        case WM_COMMAND:
            switch(LOWORD(wParam))
            {
                case IDC_SETTINGS:
                    CreateDialog(GetModuleHandle(NULL), MAKEINTRESOURCE(IDD_DIALOG3), hwnd, SettingsDlgProc);
                    break;
                case IDC_RESULTS:
                    /* CreateDialog(GetModuleHandle(NULL), MAKEINTRESOURCE(IDD_DIALOG4), hwnd, RecordsDlgProc); - Nerealizuota sioje ver. */
                    DisplayRecord(g_lvlpath);
                    break;
                case IDC_ABOUT:
                    CreateDialog(GetModuleHandle(NULL), MAKEINTRESOURCE(IDD_DIALOG2), hwnd, AboutDlgProc);
                    break;
                /* New game; New Classic game; Load Level; Exit; Continue */
                case IDC_LOADLEVEL:
                {
                    char filterbuf[64], dialogcaption[64], szName[MAX_PATH], x, len;
                    OPENFILENAME ofn;

                    LoadString(GetModuleHandle(NULL), IDS_LVLFILTERSTR, filterbuf, 64); /* Filtrai */
                    LoadString(GetModuleHandle(NULL), IDS_LOADLVLCAPT, dialogcaption, 64); /* Dialogo lango caption */

                    /* Cia kad nebutu nesusipratimu del stringu terminatoriu, kai jie saugomi resursuose */
                    len = strlen(filterbuf);
                    for(x = 0; x < len; x++){
                        if(filterbuf[x] == '_'){
                            filterbuf[x] = '\0';
                        }
                        /* putchar(filterbuf[x]); */
                    }

                    /* Cia yra Open File standartinio dialogo struktura */
                    ZeroMemory(&ofn,sizeof(OPENFILENAME));
                    ofn.lStructSize = sizeof(ofn);
                    ofn.hwndOwner = hwnd;
                    ofn.lpstrFilter = filterbuf;
                    ofn.nFilterIndex=1;
                    ofn.nMaxFile = sizeof(szName);
                    ofn.Flags = OFN_EXPLORER;
                    ofn.lpstrTitle = dialogcaption;
                    ofn.lpstrFile = szName;
                    ofn.lpstrFile[0] = '\0';
                    ofn.Flags = OFN_PATHMUSTEXIST|OFN_FILEMUSTEXIST|OFN_NOCHANGEDIR;
                    ofn.lpstrInitialDir = ".\\levels";

                    /* Rodau dialoga, gaunu rezultata */
                    if (GetOpenFileName((LPOPENFILENAME)&ofn)){
                        HWND hEdtFName;
                        strcpy(g_lvlpath, szName);
                        /* printf("Pairinktas failas: %s \n", g_lvlpath); */
                        if(!loadlevel(g_lvlpath)){
                            error(IDS_ELVLLDERR, NULL, GetModuleHandle(NULL));
                        }
                        else EnableWindow(GetDlgItem(hwnd, MAKEINTRESOURCE(IDC_CONT)), FALSE); /* Isjungiamas "Testi" */
                    }

                }
                    break;

                default:
                    PostQuitMessage(LOWORD(wParam));
                    DestroyWindow(hwnd);

                break;

               /* case IDCANCEL:
                    EndDialog(hwnd, IDCANCEL);
                break; */
            }
        break;
        default:
            return FALSE;
    }
    return TRUE;

}

/* =======================================[ Pabaiga: Pagrindinis Meniu ]============================================== */

/* "Apie..." dialogo procedura */
BOOL CALLBACK AboutDlgProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam){
    /* static HANDLE hIcon; */
    switch(msg){
        case WM_INITDIALOG:
            /* Ikraunama statiskai, todel nebereikia situ eiluciu: */
            /* hIcon = LoadIcon(GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_ICON1));
               hIcon = LoadImage(GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_ICON1), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
               printf("IconHandle: 0x%X\n", hIcon);
               SendDlgItemMessage(hwnd, IDC_SICON, STM_SETICON, IMAGE_ICON, hIcon); */
            break;
        case WM_COMMAND:
            switch(LOWORD(wParam))
            {
                case IDOK:
                    DestroyWindow(hwnd);
                    break;
            }
            break;
        /* Ir situ nebereikia, nes ikona kraunama statiskai */
        /* case WM_DESTROY:
            DeleteObject(hIcon);
            break; */
        default: return FALSE;
    }
    return TRUE;
}

/* ----------------------------------------------------------------------------------------------
    Funkcija  : Music()
    Aprasymas : Jei galima, paleidzia zaidimo muzika. Muzikai naudoja MCI. Atidaromi garsu failai.
   ---------------------------------------------------------------------------------------------- */
void Music(void){
    char muserr_cap[40]="", err[256];
    DWORD errcode;

    LoadString(GetModuleHandle(NULL), IDS_MUSERRCAP, muserr_cap, 40); /* Klaidos pranesimo caption. Kad useris susigaudytu, kur beda. */

    mciSendStringA("close music", NULL, 0, NULL);

    if(sets.muson){
        if(sets.custommus){
            char cmd[MAX_PATH + 64];
            sprintf(cmd, "open %s alias music", sets.musfile);
            errcode = mciSendStringA(cmd, NULL, 0, NULL);
        }
        else{
            errcode = mciSendStringA("open res\\music.mp3 alias music", NULL, 0, NULL);
        }

        if(errcode){
            mciGetErrorString(errcode, err, 256);
            MessageBoxA(NULL, err, muserr_cap, MB_ICONERROR | MB_OK);
        }

        mciSendStringA("play music repeat", NULL, 0, NULL);
    }
}
