/* ===================================================================================
    GYVATE v1.0 (aprasyma ziureti main.c faile arba dokumentacijoje)
   ===================================================================================
    FAILAS: BOOLEAN.H
    PASKIRTIS: Apraso BOOL tipa C kalboje. Reikalingas, kadangi standartine C kalba jo
    neturi.
   -----------------------------------------------------------------------------------
    Kompiliatorius: gcc (TDM-2 mingw32) 4.4.1
    Faustas - Azuolas BAGDONAS, 2011-12
   ===================================================================================  */
#ifndef __BOOLEAN_H
    #define __BOOLEAN_H

    #define TRUE  1
    #define FALSE 0
    typedef int BOOL;
#endif
