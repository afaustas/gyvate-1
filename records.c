/* ===================================================================================
    GYVATE v1.0 (aprasyma ziureti main.c faile arba dokumentacijoje)
   ===================================================================================
    FAILAS: RECORDS.H
    PASKIRTIS: Kas susije su rekordais, ju gavimu, saugojimu, rodymu.
   -----------------------------------------------------------------------------------
    Kompiliatorius: gcc (TDM-2 mingw32) 4.4.1
    Faustas - Azuolas BAGDONAS, 2011-12
   ===================================================================================  */
#include "records.h"

/* Naujo rekordo ivedimo dialogo procedura. Uzklausiamas surinkusiojo vardas. */
BOOL CALLBACK RecordDlgProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam){
    switch(msg){
        /* Dialogo initializacijos metu i dialogo statinius control'us irasomas kelias iki lygio failo ir rekordas */
        case WM_INITDIALOG:
            {
                HWND hCtrl;
                char cscore[6];
                DWORD errcode;
                hCtrl = GetDlgItem(hwnd, MAKEINTRESOURCE(IDC_SLEVEL));
                SetWindowText(hCtrl, g_lvlpath);
                hCtrl = GetDlgItem(hwnd, MAKEINTRESOURCE(IDC_SRECORD));
                itoa(score, cscore, 10);
                SetWindowText(hCtrl, cscore);

                /* Atidaromas garsas, kuris pasigirsta sumusus rekorda */
                mciSendStringA("open res\\record.wav type waveaudio alias record", NULL, 0, NULL);
                /* mciSendStringA("seek record to start", NULL, 0, NULL); */
                if(sets.sndon) mciSendStringA("play record", NULL, 0, NULL);
            }
            break;
        case WM_COMMAND:
            switch(LOWORD(wParam))
            {
                case ID_OK: /* Paspaudus OK saugoma informacija */
                    {
                        RECORD record;
                        HWND hEdit;
                        record.record = score;

                        hEdit = GetDlgItem(hwnd, MAKEINTRESOURCE(IDC_ENAME));
                        record.name[0] = 48;
                        record.name[SendMessage(hEdit, EM_GETLINE, 0, (LPARAM)record.name)] = 0;

                        if(!SetRecord(&record, g_lvlpath)){
                            error(IDS_ECANTSETREC, hwnd, GetModuleHandle(NULL));
                        }
                    }
                    //printf("WM_COMMAND iskviestas");
                    mciSendStringA("close record", NULL, 0, NULL);
                    EndDialog(hwnd, 0);
                    break;
            }
            break;
        default: return FALSE;
    }
    return TRUE;
}

/* ----------------------------------------------------------------------------------------------
    Funkcija  : GetRecord()
    Argumentas: *lev_path - pilnas kelias iki lygio failo (iskaitant failo varda).
    Grazina   : Gauto iraso struktura.
    Aprasymas : Nuskaito rekorda is failo.
   ---------------------------------------------------------------------------------------------- */
RECORD GetRecord(char *lev_path){
    FILE *f;
    RECORD record;
    BOOL success = FALSE;

    record.record = 0;
    record.name[0]=(char)(1); /* Klaidos atveju bus toks - rekordo nera */

    strcpy(&lev_path[strlen(lev_path) - 3], "REC");
    if(f = fopen(lev_path, "rb")) {
        char testb[2];
        fread(testb, 1, 2, f);
        if(memcmp(testb, "GR", 2) == 0){
            fread(&record, sizeof(RECORD), 1, f);
            //success = TRUE;
        }
        fclose(f);
    }


    strcpy(&lev_path[strlen(lev_path) - 3], "GLV"); /* Kolkas vartotojas gali nurodyti tik GLV lygiu failus. Neplanuojama, kad bus kitaip */
    return record;
}

/* ----------------------------------------------------------------------------------------------
    Funkcija  : SetRecord()
    Argumentas: *lev_path - pilnas kelias iki lygio failo (iskaitant failo varda).
    Grazina   : TRUE, jei pavyko
    Aprasymas : Iraso rekorda i faila.
    ---------------------------------------------------------------------------------------------- */
BOOL SetRecord(RECORD *record, char *lev_path){
    FILE *f;
    BOOL success = FALSE;

    strcpy(&lev_path[strlen(lev_path) - 3], "REC");
    if(f = fopen(lev_path, "w+b")) {
        char testb[2];
        fwrite("GR", 1, 2, f);
        fwrite(record, sizeof(RECORD), 1, f);
        fclose(f);
        success = TRUE;
        /* printf("Issaugotas rec: %s\n", lev_path); */
    }

    strcpy(&lev_path[strlen(lev_path) - 3], "GLV");
    return success;
}

/* ----------------------------------------------------------------------------------------------
    Funkcija  : DisplayRecord()
    Argumentas: *lev_path - pilnas kelias iki lygio failo (iskaitant failo varda).
    Aprasymas : Parodo dabartini rekorda.
   ---------------------------------------------------------------------------------------------- */
void DisplayRecord(char *lev_path){
    RECORD rec = GetRecord(lev_path);
    char rec_str[128], rec_strfilled[256], rec_caption[40]="";
    HINSTANCE hInstance = GetModuleHandle(NULL);

    LoadString(hInstance, IDS_RECCAPTION, rec_caption, 40);

    if(rec.name[0] == 1){
        if(!LoadString(hInstance, IDS_RECNOEXIST, rec_str, 256)){
            strcpy(rec_str, "(def) Rekordo dar nera.");
        }
        MessageBoxA(NULL, rec_str, rec_caption, MB_ICONINFORMATION | MB_OK);
        return;
    }
    if(!LoadString(hInstance, IDS_RECEXIST, rec_str, 256)){
        strcpy(rec_str, "(def) Lygyje %s\nRekordas: %d\nRekordininkas: %s\n");
    }
    sprintf(rec_strfilled, rec_str, lev_path, rec.record, rec.name);
    MessageBoxA(NULL, rec_strfilled, rec_caption, MB_ICONINFORMATION | MB_OK);
}
