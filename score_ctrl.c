/* ===================================================================================
    GYVATE v1.0 (aprasyma ziureti main.c faile arba dokumentacijoje)
   ===================================================================================
    FAILAS: SCORE_CTRL.C
    PASKIRTIS: Tasku control'as. Jo sukurimas, ir funkcijos.
   -----------------------------------------------------------------------------------
    Kompiliatorius: gcc (TDM-2 mingw32) 4.4.1
    Faustas - Azuolas BAGDONAS, 2011-12
   ===================================================================================  */
#include "score_ctrl.h"

/* ----------------------------------------------------------------------------------------------
    Funkcija  : InitScoreCtrl()
    Grazina   : TRUE, jei pasiseke initializuoti.
    Aprasymas : Apibrezia Score Control.
   ---------------------------------------------------------------------------------------------- */
BOOL InitScoreCtrl(void){
    WNDCLASSEX wce;

    if (!LoadString(GetModuleHandle(NULL), IDS_SCORESTR, g_scoreword, 16)){
        strcpy(g_scoreword, " Score: ");
    }

    wce.cbClsExtra = 0; /* Strukturoje papildomu duomenu nebus */
    wce.cbSize = sizeof(WNDCLASSEX);
    wce.cbWndExtra = 0; /* Papildomu baitu nereikia skirti kiekvienam sios klases langui */
    wce.hbrBackground = (HBRUSH)(COLOR_WINDOW);
    wce.hCursor = LoadCursor(NULL, IDC_ARROW);
    wce.hIcon = NULL;
    wce.hIconSm = NULL;
    wce.hInstance = GetModuleHandle(0); /* Tik siai programos instancijai */
    wce.lpfnWndProc = ScoreCtrlProc;
    wce.lpszClassName = SCORE_CTRL_CLASS_NAME;
    wce.lpszMenuName = NULL;
    wce.style = CS_GLOBALCLASS;

    return RegisterClassEx(&wce) && TRUE;

}

/* Score Control'o procedura */
LRESULT CALLBACK ScoreCtrlProc (HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam){
    static HBRUSH hScBmpBg, hScBmpFg;
    static HBRUSH hScBrFg, hScBrBg;

    switch (uMsg)  {

        case WM_PAINT: /* Piesimas ant Score Control. Spalvinamas bitmapiniais brushais. */
         {
            PAINTSTRUCT ps;
            HDC hDC;
            RECT rect;

            hDC = BeginPaint(hWnd, &ps);
            GetClientRect(hWnd, &rect);

            FillRect(hDC, &rect, hScBrBg);

            rect.bottom -= 2;
            rect.left += 2;
            rect.right -= 2;
            rect.top += 2;

            DrawEdge(
                hDC,
                &rect,
                EDGE_BUMP,
                BF_MIDDLE | BF_RECT | BF_SOFT
                );

            FillRect(hDC, &rect, hScBrFg);

            PaintOnScore(score,0,hWnd, hDC);
            EndPaint(hWnd, &ps);
        }
        break;

        case WM_DESTROY:
            {
                /* Langas jau nebematomas. Naikinamas. */
                /* Lango naikinimo metu turiu sunaikinti ir jo naudotus bitmapus ir brushus */
                DeleteObject(hScBrBg);
                DeleteObject(hScBmpBg);
                DeleteObject(hScBrFg);
                DeleteObject(hScBmpFg);
            }
            break;

        case WM_CREATE: /* Sukuriamas control'as */
            {
                /* Bitmapais spalvinimui sukuriami brush'ai */
                hScBmpBg = (HBITMAP)LoadImage(NULL, "res\\sbg.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE); /* Background ikraunu */
                hScBrBg = CreatePatternBrush(hScBmpBg);
                hScBmpFg = (HBITMAP)LoadImage(NULL, "res\\sfg.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE); /* Foreground ikraunu */
                hScBrFg = CreatePatternBrush(hScBmpFg);

                /* CreateWindow("button", - Cia du mygtukai, kuriu nusprendziau, kad nereikia
                             "",
                             WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON|BS_ICON,
                             300,
                             5,
                             15,
                             15,
                             hWnd,
                             (HMENU) IDB_SOUND, /* Mygtuko ID nustatomas per MENU handleri *
                             GetModuleHandle(0), NULL);

                CreateWindow("button",
                             "Meniu", ///localization!!!
                             WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
                             316,
                             5,
                             50,
                             15,
                             hWnd,
                             (HMENU) IDB_MENU, /* Mygtuko ID nustatomas per MENU handleri *
                             GetModuleHandle(0), NULL);  */

            }
            break;

        default: return DefWindowProc(hWnd, uMsg, wParam, lParam); /* Defaultinis vykdymas. Tai atlieka OS, nes neaprasyta. */
    }
    return 0;
}

/* ------------------------------------------------------------------- *
    Funkcija:   PaintOnScore()
    Argumentai: unsigned int score - taskai
                unsigned int bonust - laikas iki bonuso galiojimo pabaigos (nerealizuota sioje versijoje)
                HWND hScoreWnd - Lango handleris
                HDC hDC - lango DC. Jei neturiu, NULL.
    Paskirtis:  Atnaujinti ir perpiesti score komponenta. Irasomas score,
                kuris perduodamas per %score% kintamaji.
 * ------------------------------------------------------------------- */
void PaintOnScore(unsigned int score, unsigned int bonust, HWND hScoreWnd, HDC hDCScore){
    RECT SWndRect;
    HFONT hFont, hDefFont;
    char scorestr[24];
    HDC hDC  = hDCScore;

    GetClientRect(hScoreWnd, &SWndRect);

    /* Jeigu DC nebuvo perduotas, tai gaunu ji */
    if(hDC == NULL){
        if((hDC = GetDC(hScoreWnd)) == NULL){
          /* Klaida: negavo dc!!! */
          return;
        }
        /* Tikrai kvieciama ne is WM_PAINT, reikia perpiesti */
        /* Kadangi kviesti WM_PAINT tiesiogiai yra bloga praktika, darau
           tai panaudodamas tris windows funkcijas                         */
           RefreshWindow(hScoreWnd);
    }

    /* Formatuoju tasku stringa */
    wsprintf(scorestr, " %s: %u", g_scoreword, score);

    /* Parenku fonta */
    hFont = CreateFont(20, /* Height, em */
                       20, /* Width */
                       0, /* Escapement */
                       0, /* Orentation */
                       FW_NORMAL, /* Ant kiek bold */
                       FALSE, /* Italic */
                       FALSE, /* Underline */
                       FALSE, /* No strikeout */
                       DEFAULT_CHARSET, /* Charset - pagal regional settings */
                       OUT_RASTER_PRECIS, /* Jei toks yra ir raster ir vector, renkuosi raster - lengviau renderint */
                       CLIP_DEFAULT_PRECIS,
                       DEFAULT_QUALITY, /* Priklausomai nuo vartotojo nustatymu */
                       FF_SCRIPT, //!!!!
                       "Courier New");

    hDefFont = SelectObject(hDC, hFont);

    /* O cia piesiu ant control */
    SetTextColor(hDC, RGB(255,255,0));
    //SetBkColor((HDC)hDC, RGB(0,255,255));
    SetBkMode(hDC, TRANSPARENT);
    if(DrawText((HDC)hDC, scorestr, strlen(scorestr), &SWndRect, DT_LEFT|DT_VCENTER|DT_SINGLELINE))

    SelectObject(hDC, hDefFont);
    DeleteObject(hFont);

    /* Jeigu as sioje funkcijoje pasiemiau DC, tai baiges turiu ji atlaisvinti */
    if (hDCScore == NULL)
        ReleaseDC(hScoreWnd, hDC);
}

/* ----------------------------------------------------------------------------------------------
    Funkcija  : create_status()
    Argumentas: hParent - Tevinio lango handleris.
    Grazina   : Handleri i score control. Nesekmingu atveju NULL.
    Aprasymas : Sukuria score control.
   ---------------------------------------------------------------------------------------------- */
HWND create_status(HWND hParent){
    HWND hWnd;             /* Lango handleris */

    hWnd = CreateWindowEx(
           0,
           SCORE_CTRL_CLASS_NAME, //"STATIC", //g_szStatusClassName,         /* Lango klases vardas */
           "",       /* Caption */
           WS_VISIBLE|WS_CHILD, //SS_OWNERDRAW, /* Langas matomas*/
           0,       /* Horizontali lango pozicija*/
           455,       /* Vertikali lango pozicija */
           640,                 /* Width */
           25,                 /* Height */
           hParent,        /* Langas yra pagrindiniame lange */
           (HMENU)IDC_SCORE,                /* Per meniu perduodu control ID */
           GetModuleHandle(0),           /* Programos instancija */
           NULL                 /* Nera papildomu duomenu langui*/
           );

    if(hWnd == NULL){
        error(IDS_ENOSCOREW, hParent, GetModuleHandle(NULL));
        /* MessageBoxA(NULL, "err: create_status()", "err",MB_OK); */
    }
    return hWnd;

}

/* ----------------------------------------------------------------------------------------------
    Funkcija  : RefreshWindow()
    Argumentas: hWnd - lango, kuri reikia refreshint, handleris.
    Aprasymas : Refreshina langa.
   ---------------------------------------------------------------------------------------------- */
void RefreshWindow(HWND hWnd){
    RECT rect;

    GetClientRect(hWnd, &rect);
    InvalidateRect (hWnd, &rect, TRUE);
    UpdateWindow (hWnd);
}


